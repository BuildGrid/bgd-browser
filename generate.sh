#!/usr/bin/env bash
cd "$(dirname $0)"
rm -rf src/protos/*
rm -rf server/src/protos/*
npx protoc --proto_path=protos --ts_out src/protos $(find protos -name '*.proto' -type f)
cp -r src/protos/* server/src/protos/
