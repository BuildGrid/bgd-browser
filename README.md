# BuildGrid Browser

A web-based UI for browsing Operations, Actions and CAS content in a
BuildGrid deployment.

This tool uses a part of the BuildGrid command line tooling to communicate
with the remote BuildGrid instance, so that needs to be running somewhere
with access to the remote BuildGrid. For example,

``` sh
bgd browser-backend --remote=http://buildgrid:50051 serve
```

A docker-compose example is provided which sets up a BuildGrid, with this
backend available.


## Features

- Support for browsing and filtering Operations in a BuildGrid instance
    - See [the BuildGrid documentation][0] for search syntax
- Viewing output from and cached results of completed Actions
- Viewing live logs of in-progress Actions
- Viewing directories and files in CAS (with known digests)
- Viewing operations and metadata for invocations and correlated invocations
- [New] Viewing results from multiple BuildGrid setups.

[0]: https://buildgrid.build/user/components.html#listoperations-filtering-and-sorting


## Configuration

### Single BuildGrid Setup

bgd-browser needs to be pointed at the `bgd browser-backend` API using either
build-time environment variables or its runtime config file. The following
environment variables are available,

- `VITE_BACKEND_URL` - Should point to the host/port that `bgd browser-backend`
  is served on, eg. `http://localhost:8080`
- `VITE_LOGSTREAM_URL` - Should point to the websocket route served by
  `bgd browser-backend`, eg. `ws://localhost:8080/ws/logstream`

These can be set in the build environment, or in [a .env file][1].

[1]: https://cli.vuejs.org/guide/mode-and-env.html

If build-time config isn't sufficient, then these environment variables can be
left unset and a custom version of `public/config.json` provided. This file is
loaded when the app starts up (its expected to be served at `/config.json`),
and its contents used when the build time env vars were unset.

### Multi BuildGrid Setup

If you wish to create views for multiple BuildGrid instances, you can do so using
the `public/config.json` file. A top level key named `"namespaces"` indicates that
there will be multiple instances configured from the single UI. The value for the
key is an object, where the keys are names of the BuildGrid instances you wish to
display, and the values are the normal schema of a single instance found in 
`public/config.json`. When using this mode, the `VITE_BACKEND_URL` and
`VITE_LOGSTREAM_URL` values have no effect.

## docker-compose example

This repo includes an example docker-compose setup containing a BuildGrid
instance using buildbox-run-hosttools on the worker (with FUSE used to stage
files by buildbox-casd). Usage of the example should be simple,

```
docker-compose up --build
```

BuildGrid Browser is served at `http://localhost/`. The BuildGrid instance
itself is at `http://localhost:50051`. Send some Actions to it to view them
in the browser.


## Quick Usage

### Install dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
