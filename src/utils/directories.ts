import bytestream from "@/apis/bytestream";
import {
  Digest,
  Directory,
  DirectoryNode,
  FileNode,
  OutputDirectory,
  OutputFile,
  OutputSymlink,
  SymlinkNode,
} from "@/protos/build/bazel/remote/execution/v2/remote_execution";

export interface DirectoryTreeNode {
  name: string;
  digest?: Digest;
  proto?: Directory;
}

export interface FileTreeNode {
  name: string;
  digest?: Digest;
}

export interface SymlinkTreeNode {
  name: string;
  target: string;
}

export interface DirectoryView {
  directories: DirectoryTreeNode[];
  files: FileTreeNode[];
  symlinks: SymlinkTreeNode[];
}

interface Outputs {
  directories: OutputDirectory[];
  files: OutputFile[];
  symlinks: OutputSymlink[];
}

async function normalizeDirectory(
  dir: OutputDirectory | DirectoryNode,
  namespace: string
): Promise<DirectoryTreeNode> {
  const digest = "digest" in dir ? (dir as DirectoryNode).digest : (dir as OutputDirectory).rootDirectoryDigest;
  let proto = digest
    ? await bytestream.readDirectory(namespace, digest.hash, digest.sizeBytes)
    : undefined;

  if (Object.hasOwn(dir, "treeDigest") && digest === undefined) {
    const treeDigest = (dir as OutputDirectory).treeDigest;
    if (treeDigest !== undefined) {
      try {
        // TODO: This also contains the entire directory tree, which currently we just
        // throw away without using. We can optimize the tree view components by storing
        // the whole tree here, and reusing it later.
        const tree = await bytestream.readTree(namespace, treeDigest.hash, treeDigest.sizeBytes);
        proto = tree.root;
      } catch (_) { }
    }
  }
  const name = "digest" in dir ? (dir as DirectoryNode).name : (dir as OutputDirectory).path;
  return {
    name,
    digest,
    proto,
  };
}

function normalizeFile(file: OutputFile | FileNode): FileTreeNode {
  return {
    name: "name" in file ? (file as FileNode).name : (file as OutputFile).path,
    digest: file.digest,
  };
}

function normalizeSymlink(link: OutputSymlink | SymlinkNode): SymlinkTreeNode {
  return {
    name: "name" in link ? (link as SymlinkNode).name : (link as OutputSymlink).path,
    target: link.target,
  };
}

export async function getDirectoryView(
  dir: Directory | Outputs,
  namespace: string
): Promise<DirectoryView> {
  // TODO: Consider BatchReadBlobs in the Directory case when we have a backend
  // route for that, rather than reading each child Directory individually
  const directories = await Promise.all(
    dir.directories.map(async (d) => await normalizeDirectory(d, namespace))
  );
  return {
    directories,
    files: dir.files.map(normalizeFile),
    symlinks: dir.symlinks.map(normalizeSymlink),
  };
}
