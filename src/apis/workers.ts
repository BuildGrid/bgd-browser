import { Digest } from "@/protos/build/bazel/remote/execution/v2/remote_execution";
import { ListWorkersResponse } from "@/protos/buildgrid/v2/introspection";
import { BotStatus } from "@/protos/google/devtools/remoteworkers/v1test2/bots";
import { Timestamp } from "@/protos/google/protobuf/timestamp";
import { getArrayBuffer, getConfig } from "./base";

export interface ParsedWorker {
  sessionName: string;
  workerName: string;
  botStatus: BotStatus;
  operations: string[];
  actionDigest?: Digest;
  lastUpdated?: Date;
  expiryTime?: Date;
}

export interface ParsedListWorkersResponse {
  page: number;
  pageSize: number;
  total: number;
  workers: ParsedWorker[];
}

function dateFromTimestamp(timestamp: Timestamp): Date {
  return new Date((Number(timestamp.seconds) * 1000) + (timestamp.nanos * 0.000001))
}

export default {
  async list(namespace: string, workerName: string, page: number, pageSize: number): Promise<ParsedListWorkersResponse> {
    const params = new URLSearchParams({
      page: page.toString(),
      page_size: pageSize.toString(),
      worker_name: workerName
    });
    const url = `${getConfig(namespace).backendUrl}/api/v1/workers`
    const data = ListWorkersResponse.fromBinary(await getArrayBuffer(url, params));
    return {
      ...data,
      workers: data.workers.map(worker => {
        return {
          sessionName: worker.sessionName,
          workerName: worker.workerName,
          botStatus: worker.botStatus,
          operations: worker.operations.map(op => op.operationName),
          actionDigest: worker.actionDigest,
          lastUpdated: worker.lastUpdated ? dateFromTimestamp(worker.lastUpdated) : undefined,
          expiryTime: worker.expiryTime ? dateFromTimestamp(worker.expiryTime) : undefined
        }
      })
    }
  }
}
