/**
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import {
  Action,
  Command, Digest,
  Directory,
  Tree
} from '@/protos/build/bazel/remote/execution/v2/remote_execution'

import { getConfig, getArrayBuffer, getText } from '@/apis/base'
import { DecodedAction, decodeAction } from '@/apis/actions'
import { Any } from '@/protos/google/protobuf/any'

export default {
  async readBlob (namespace: string, hash: string, sizeBytes: bigint | string): Promise<string> {
    return await getText(this.getBlobUrl(namespace, hash, sizeBytes))
  },
  async readAction (namespace: string, hash: string, sizeBytes: bigint | string): Promise<DecodedAction> {
    const action = Action.fromBinary(await getArrayBuffer(this.getBlobUrl(namespace, hash, sizeBytes)))
    return decodeAction(action);
  },
  async readCommand (namespace: string, hash: string, sizeBytes: bigint | string): Promise<Command> {
    return Command.fromBinary(await getArrayBuffer(this.getBlobUrl(namespace, hash, sizeBytes)))
  },
  async readDirectory (namespace: string, hash: string, sizeBytes: bigint | string): Promise<Directory> {
    return Directory.fromBinary(await getArrayBuffer(this.getBlobUrl(namespace, hash, sizeBytes)))
  },
  async readTree (namespace: string, hash: string, sizeBytes: bigint | string): Promise<Tree> {
    return Tree.fromBinary(await getArrayBuffer(this.getBlobUrl(namespace, hash, sizeBytes)))
  },
  async readAny (namespace: string, hash: string, sizeBytes: bigint | string): Promise<Any> {
    return Any.fromBinary(await getArrayBuffer(this.getBlobUrl(namespace, hash, sizeBytes)))
  },
  blobUrl (namespace: string, digest: Digest): string {
    return this.getBlobUrl(namespace, digest.hash, digest.sizeBytes)
  },
  getBlobUrl (namespace: string, hash: string, sizeBytes: bigint | string): string {
    return `${getConfig(namespace).backendUrl}/api/v1/blobs/${hash}/${sizeBytes}`
  }
}
