/**
  Copyright 2021 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import {
  CheckIcon, CogIcon, DocumentSearchIcon, PauseIcon, QuestionMarkCircleIcon, StopIcon, XIcon
} from '@heroicons/vue/outline'

import {
  Command,
  ExecuteOperationMetadata,
  ExecuteResponse,
  ExecutionStage_Value as ExecutionStageValue,
  LogFile,
  RequestMetadata
} from '@/protos/build/bazel/remote/execution/v2/remote_execution'
import {
  ListOperationsResponse,
  Operation
} from '@/protos/google/longrunning/operations'
import {
  Code
} from '@/protos/google/rpc/code'

import { DecodedAction, DecodedActionResult, decodeActionResult } from '@/apis/actions'
import { getArrayBuffer, getConfig } from '@/apis/base'
import bytestream from '@/apis/bytestream'
import stageName from '@/apis/reapi'
import { ClientIdentity } from '@/protos/buildgrid/v2/identity'
import { Timestamp } from '@/protos/google/protobuf/timestamp'
import { Status } from '@/protos/google/rpc/status'
import Axios from 'axios'

function dateFromTimestamp(timestamp: Timestamp): Date {
  return new Date((Number(timestamp.seconds) * 1000) + (timestamp.nanos * 0.000001))
}

export interface DecodedExecuteResponse {
  result?: DecodedActionResult;
  cachedResult: boolean;
  status?: Status;
  serverLogs: {
    [key: string]: LogFile;
  };
  message: string;
}

export function decodeExecuteResponse(executeResponse: ExecuteResponse): DecodedExecuteResponse {
  return {
    result: executeResponse.result ? decodeActionResult(executeResponse.result) : undefined,
    cachedResult: executeResponse.cachedResult,
    status: executeResponse.status,
    serverLogs: executeResponse.serverLogs,
    message: executeResponse.message
  }
}

function totalExecutionTime(executeResponse?: DecodedExecuteResponse): number | undefined {
  const queuedTimestamp = executeResponse?.result?.executionMetadata?.queuedTimestamp
  const workerCompletedTimestamp = executeResponse?.result?.executionMetadata?.workerCompletedTimestamp
  if (queuedTimestamp && workerCompletedTimestamp) {
    const start = dateFromTimestamp(queuedTimestamp)
    const end = dateFromTimestamp(workerCompletedTimestamp)
    const deltaSecs = (end.getTime() - start.getTime()) / 1000
    return deltaSecs
  }
  return undefined
}

export interface DecodedOperation {
  name: string;
  metadata?: ExecuteOperationMetadata;
  done: boolean;
  response?: DecodedExecuteResponse;
  error?: Status;
  action?: DecodedAction;
  command?: Command;
  // Flag to keep track of whether we are displaying the whole information in the view:
  expandedRowIsVisible: boolean;
  totalExecutionTime?: number;
}

export function decodeOperation(operation: Operation): DecodedOperation {
  const metadata = operation.metadata
    ? ExecuteOperationMetadata.fromBinary(new Uint8Array(operation.metadata.value))
    : undefined
  const response = 'response' in operation.result
    ? decodeExecuteResponse(ExecuteResponse.fromBinary(operation.result.response.value))
    : undefined
  return {
    name: operation.name,
    done: operation.done,
    metadata,
    response,
    error: 'error' in operation.result ? operation.result.error : undefined,
    expandedRowIsVisible: false,
    totalExecutionTime: totalExecutionTime(response)
  }
}

// If we're going to be filtering the results, it makes sense to only do this step when actually needed.
// Otherwise, for worker invocation filters you can end up with 1000's of extra calls.
export async function finalizeDecodedOperation(namespace: string, operation: DecodedOperation): Promise<DecodedOperation> {
  if (operation.metadata && operation.metadata.actionDigest) {
    try {
      operation.action = await bytestream.readAction(
        namespace, operation.metadata.actionDigest.hash, operation.metadata.actionDigest.sizeBytes)
    } catch (_) { }
  }
  if (operation.action && operation.action.commandDigest) {
    try {
      operation.command = await bytestream.readCommand(
        namespace, operation.action.commandDigest.hash, operation.action.commandDigest.sizeBytes)
    } catch (_) { }
  }
  return operation
}

export interface OperationsList {
  nextPageToken: string;
  operations: DecodedOperation[];
}

export enum OperationFilterType {
  Text = "text",
  DateTime = "datetime"
}

export interface OperationFilter {
  comparators: string[];
  description: string;
  key: string;
  name: string;
  type: OperationFilterType;
  values?: string[];
}

export default {
  operationsUrl(namespace: string, path?: string): string {
    const url = `${getConfig(namespace).backendUrl}/api/v1/operations`
    // TODO Fixme. Seems to be an issue with accepting instance names.
    // Can strip the instance name off for now...
    return url + (path ? ('/' + path.split('/').at(-1)) : '')
  },
  async list(
    namespace: string,
    filterString: string,
    options?: {
      pageSize?: number;
      pageToken?: string;
      postParseFilter?: (operation: DecodedOperation) => boolean;
    }
  ): Promise<OperationsList> {
    const params = new URLSearchParams({
      q: filterString,
      page_size: (options?.pageSize || 0).toString(),
      page_token: options?.pageToken || ''
    })
    const response = ListOperationsResponse.fromBinary(await getArrayBuffer(this.operationsUrl(namespace), params))

    return {
      nextPageToken: response.nextPageToken,
      operations: await Promise.all(response.operations
        .map(operation => decodeOperation(operation))
        .filter(operation => !options?.postParseFilter || options.postParseFilter(operation))
        .map(operation => finalizeDecodedOperation(namespace, operation))
      )
    }
  },

  async get(namespace: string, name: string): Promise<DecodedOperation> {
    const operation = Operation.fromBinary(await getArrayBuffer(this.operationsUrl(namespace, name)))
    return await finalizeDecodedOperation(namespace, decodeOperation(operation))
  },

  async getFilters(namespace: string): Promise<OperationFilter[]> {
    return await Axios.get(`${getConfig(namespace).backendUrl}/api/v1/operation_filters`)
  },

  async getRequestMetadata(namespace: string, name: string): Promise<RequestMetadata> {
    const requestMetadata = RequestMetadata.fromBinary(await getArrayBuffer(this.operationsUrl(namespace, name) + '/request_metadata'))
    return requestMetadata
  },

  async getClientIdentity(namespace: string, name: string): Promise<ClientIdentity> {
    return ClientIdentity.fromBinary(await getArrayBuffer(this.operationsUrl(namespace, name) + '/client_identity'));
  },

  /* eslint-disable @typescript-eslint/no-unsafe-return */
  async cancel(namespace: string, name: string): Promise<any> {
    try {
      await Axios.delete(this.operationsUrl(namespace, name))
    } catch (error: any) {
      if (error.response) {
        return error.response.status
      } else if (error.request) {
        return error.request
      } else {
        return error.message
      }
    }
  },
  /* eslint-enable @typescript-eslint/no-unsafe-return */

  operationIsQueued(operation: DecodedOperation) {
    return operation.metadata?.stage === ExecutionStageValue.QUEUED
  },
  operationIsExecuting(operation: DecodedOperation) {
    return operation.metadata?.stage === ExecutionStageValue.EXECUTING
  },
  operationIsCompleted(operation: DecodedOperation) {
    const wasCancelled = !operation.response
    return (operation.metadata?.stage === ExecutionStageValue.COMPLETED && !wasCancelled)
  },
  operationIsSuccessful(operation: DecodedOperation) {
    return (operation.metadata?.stage === ExecutionStageValue.COMPLETED &&
      operation.response?.result?.exitCode === 0)
  },

  operationWasCancelled(operation: DecodedOperation) {
    return operation.done && (operation.error?.code === Code.CANCELLED)
  },
  operationResultIsMissing(operation: DecodedOperation) {
    // This is set by `ListOperations()` when the `ActionResult` message
    // for an operation could not be found.
    return operation.done && (operation.error?.code === Code.DATA_LOSS)
  },
  exitCode(operation: DecodedOperation) {
    if (operation.done) {
      return operation.response?.result?.exitCode
    }
    return undefined
  },
  commandFailed(operation: DecodedOperation) {
    const ec = this.exitCode(operation)
    return (ec && ec !== 0)
  },
  stageName(operation: DecodedOperation) {
    if (this.operationWasCancelled(operation)) {
      return 'Cancelled'
    }
    const stageValue = operation.metadata?.stage
    if (this.commandFailed(operation) && stageValue === ExecutionStageValue.COMPLETED) {
      return 'Failed' // Special case: we don't want to show "COMPLETED" in red.
    }
    return stageName(stageValue)
  },
  stageColor(operation: DecodedOperation) {
    if (this.operationWasCancelled(operation) || this.operationResultIsMissing(operation)) {
      return 'var(--color-text-muted)'
    }

    switch (operation.metadata?.stage) {
      case ExecutionStageValue.QUEUED:
        return 'var(--color-operation-queued)'
      case ExecutionStageValue.EXECUTING:
        return 'var(--color-operation-executing)'
      case ExecutionStageValue.COMPLETED:
        return (this.commandFailed(operation) ? 'var(--color-operation-failure)' : 'var(--color-operation-success)')
      default:
        return 'var(--color-text)'
    }
  },
  stageIcon(operation: DecodedOperation) {
    if (this.operationWasCancelled(operation)) {
      return StopIcon
    }

    if (this.operationResultIsMissing(operation)) {
      return QuestionMarkCircleIcon
    }

    switch (operation.metadata?.stage) {
      case ExecutionStageValue.EXECUTING:
        return CogIcon
      case ExecutionStageValue.COMPLETED:
        if (this.commandFailed(operation)) {
          return XIcon
        } else {
          return CheckIcon
        }
      case ExecutionStageValue.QUEUED:
        return PauseIcon
      case ExecutionStageValue.CACHE_CHECK:
        return DocumentSearchIcon
      default:
        return QuestionMarkCircleIcon
    }
  }
}
