/**
  Copyright 2022 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import {
  QueryEventStreamsResponse
} from '@/protos/buildgrid/v2/query_build_events'

import { getConfig, getArrayBuffer } from '@/apis/base'

export async function listStreams (namespace: string, buildId: string): Promise<QueryEventStreamsResponse> {
  const config = getConfig(namespace)
  const eventsUrl = `${config.backendUrl}/api/v1/build_events`
  const params = new URLSearchParams({ build_id: buildId })
  return QueryEventStreamsResponse.fromBinary(await getArrayBuffer(eventsUrl, params))
}
