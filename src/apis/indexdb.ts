import { LocationQuery, RouteParams, RouteRecordName } from 'vue-router'

const DB_NAME = 'pagedb'
const DB_VERSION = 1
let DB: IDBDatabase

export interface RouteInfo {
  name: RouteRecordName | null | undefined;
  hash: string;
  params: RouteParams;
  query: LocationQuery;
  fullPath: string;
}

export interface Page {
  timestamp: number;
  url: string;
  route: RouteInfo;
}

export default {
  async getDb (): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      if (DB) { return resolve(DB) }
      console.log('OPENING DB', DB)
      const request = window.indexedDB.open(DB_NAME, DB_VERSION)

      request.onerror = e => {
        console.log('Error opening db', e)
        reject(e)
      }

      request.onsuccess = e => {
        DB = (e.target as IDBOpenDBRequest).result
        resolve(DB)
      }

      request.onupgradeneeded = e => {
        console.log('onupgradeneeded')
        const db = (e.target as IDBOpenDBRequest).result
        const store = db.createObjectStore('pages', { keyPath: 'url' })
        store.createIndex('timestamp', 'timestamp')
      }
    })
  },
  async deletePage (page: Page): Promise<void> {
    const db = await this.getDb()

    return new Promise(resolve => {
      const trans = db.transaction(['pages'], 'readwrite')
      trans.oncomplete = () => {
        resolve()
      }

      const store = trans.objectStore('pages')
      store.delete(page.url)
    })
  },
  async getPages (): Promise<Page[]> {
    const db = await this.getDb()

    return new Promise(resolve => {
      const trans = db.transaction(['pages'], 'readonly')
      trans.oncomplete = () => {
        resolve(pages)
      }

      const store = trans.objectStore('pages')
      const index = store.index('timestamp')

      const pages: Page[] = []

      // Read the pages from the database in reverse timestamp order.
      index.openCursor(null, 'prev').onsuccess = e => {
        const cursor = (e.target as IDBRequest<IDBCursorWithValue>).result
        if (cursor) {
          pages.push(cursor.value as Page)
          cursor.continue()
        }
      }
    })
  },
  async savePage (page: Page): Promise<void> {
    const db = await this.getDb()

    return new Promise(resolve => {
      const trans = db.transaction(['pages'], 'readwrite')
      trans.oncomplete = () => {
        resolve()
      }

      const store = trans.objectStore('pages')
      store.put(page)
    })
  }
}
