/**
  Copyright 2021 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import {
  Action,
  ActionResult, Digest, ExecutedActionMetadata, OutputDirectory, OutputFile, OutputSymlink, Platform
} from '@/protos/build/bazel/remote/execution/v2/remote_execution'
import { Duration } from '@/protos/google/protobuf/duration';

import { getConfig, getArrayBuffer } from '@/apis/base'

function u8ToString (u8: Uint8Array): string {
  return new TextDecoder('utf-8').decode(u8)
}

export interface DecodedActionResult {
  outputFiles: OutputFile[];
  outputFileSymlinks: OutputSymlink[];
  outputSymlinks: OutputSymlink[];
  outputDirectories: OutputDirectory[];
  outputDirectorySymlinks: OutputSymlink[];
  exitCode: number;
  stdoutRaw: string;
  stdoutDigest?: Digest;
  stderrRaw: string;
  stderrDigest?: Digest;
  executionMetadata?: ExecutedActionMetadata;
}

export interface DecodedAction {
  commandDigest?: Digest;
  inputRootDigest?: Digest;
  timeout?: Duration;
  doNotCache: boolean;
  salt: string;
  platform?: Platform;
}

export function decodeAction (action: Action): DecodedAction {
  return {
    commandDigest: action.commandDigest,
    inputRootDigest: action.inputRootDigest,
    timeout: action.timeout,
    doNotCache: action.doNotCache,
    salt: action.salt.length > 0 ? u8ToString(action.salt) : '',
    platform: action.platform
  }
}

export function decodeActionResult (actionResult: ActionResult): DecodedActionResult {
  return {
    outputFiles: actionResult.outputFiles,
    outputFileSymlinks: actionResult.outputFileSymlinks,
    outputSymlinks: actionResult.outputSymlinks,
    outputDirectories: actionResult.outputDirectories,
    outputDirectorySymlinks: actionResult.outputDirectorySymlinks,
    exitCode: actionResult.exitCode,
    stdoutRaw: actionResult?.stdoutRaw.length > 0 ? u8ToString(actionResult.stdoutRaw) : '',
    stdoutDigest: actionResult.stdoutDigest,
    stderrRaw: actionResult?.stderrRaw.length > 0 ? u8ToString(actionResult.stderrRaw) : '',
    stderrDigest: actionResult.stderrDigest,
    executionMetadata: actionResult.executionMetadata
  }
}

export async function getResult (namespace: string, hash: string, sizeBytes: bigint | string): Promise<DecodedActionResult> {
  const config = getConfig(namespace)
  const resultsUrl = `${config.backendUrl}/api/v1/action_results/${hash}/${sizeBytes}`
  return decodeActionResult(ActionResult.fromBinary(await getArrayBuffer(resultsUrl)))
}
