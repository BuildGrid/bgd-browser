/**
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import Axios from 'axios';

export interface NamespaceConfig {
  allowOperationCancelling: boolean;
  description: string;
  logStreamUrl: string;
  backendUrl: string;
}

export interface Config {
  namespaces: { [key: string]: NamespaceConfig }
}

const config: Config = ({} as Config)

const DEFAULT_NS_CONFIG = {
  allowOperationCancelling: false,
  description: "Default fallback namespace",
  ...generateUrlsForNamespace('')
}

function generateUrlsForNamespace(namespace: string) {
  const url = new URL(location.href)
  let webSocketProtocol = 'ws'
  if (url.protocol === 'https:') {
    webSocketProtocol = 'wss'
  }
  const endpoint = url.port ? `${url.hostname}:${url.port}` : url.hostname
  if (!!namespace) {
    return {
      logStreamUrl: `${webSocketProtocol}://${endpoint}/${namespace}/ws/logstream`,
      backendUrl: `/${namespace}`,
    }
  }
  return {
    logStreamUrl: `${webSocketProtocol}://${endpoint}/ws/logstream`,
    backendUrl: '',
  }
}

export async function loadConfig() {
  const { data } = await Axios.get<any, { data: Config }>('/api/config')
  config.namespaces = Object.fromEntries(Object.entries(data.namespaces).map(([namespace, conf]) => {
    return [
      namespace,
      {
        ...conf,
        ...generateUrlsForNamespace(namespace)
      }]
  }));
}

export function getConfig(namespace: string): NamespaceConfig {
  if (!config.namespaces) {
    loadConfig();
  }

  return config.namespaces[namespace] || DEFAULT_NS_CONFIG;
}

export function getNamespaces(): string[] | undefined {
  return ('namespaces' in config) ? Object.keys(config.namespaces) : undefined
}

export async function getText(url: string, params?: URLSearchParams): Promise<string> {
  const { data } = await Axios.get<any, { data: string }>(url, { params, responseType: 'text' })
  return data
}

export async function getArrayBuffer(url: string, params?: URLSearchParams): Promise<Uint8Array> {
  const { data } = await Axios.get<any, { data: ArrayBuffer }>(url, { params, responseType: 'arraybuffer' })
  return new Uint8Array(data)
}
