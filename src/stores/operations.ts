/**
  Copyright 2022 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

import { defineStore } from 'pinia'
import { LRUCache } from 'mnemonist'

import operations, { DecodedOperation } from '@/apis/operations'
import { useActionResultCache } from './actionresults'

function maybeStoreResult (namespace: string, operation: DecodedOperation) {
  const actionResults = useActionResultCache()
  const result = operation?.response?.result
  if (result) {
    const digest = operation.metadata?.actionDigest
    if (digest) {
      actionResults.storeActionResult(namespace, digest.hash, digest.sizeBytes, result)
    }
  }
}

export const useOperationsCache = defineStore('operations', {
  state: () => {
    return {
      ongoingFetches: {} as {[key: string]: Promise<DecodedOperation>},
      operations: new LRUCache<string, DecodedOperation>(128)
    }
  },
  actions: {
    async fetchOperation (namespace: string, name: string) {
      // TODO: add a TTL to this cached result
      if (this.operations.has(name) && this.operations.get(name)?.done) {
        return this.operations.get(name)
      }

      // Deduplicate the actual fetch promise, just in case
      if (!(name in this.ongoingFetches)) {
        this.ongoingFetches[name] = operations.get(namespace, name)
      }
      try {
        const operation = await (this.ongoingFetches[name])
        this.operations.set(name, operation)
        maybeStoreResult(namespace, operation)
        return operation
      } finally {
        delete this.ongoingFetches[name]
      }
    },
    storeOperation (namespace: string, operation: DecodedOperation) {
      if (operation) {
        maybeStoreResult(namespace, operation)
        this.operations.set(operation.name, operation)
      }
    }
  }
})
