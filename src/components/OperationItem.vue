<!--
  Copyright 2021 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<template>
  <div
    class="operation"
    :class="{ 'expanded-operation': expandedRowIsVisible }"
  >
    <div class="operations-summary">
      <component
        :is="stageIcon"
        class="stage-icon"
        :style="{ color: stageColor }"
      />

      <div
        class="operation-summary-info"
        @click="toggleExpandedRow()"
      >
        <div class="operation-info-row">
          <div
            v-if="operation.command"
            class="command-listing"
          >
            <span
              v-for="argument in operation.command.arguments"
              :key="argument"
              class="command-argument"
            >
              {{ argument }}
            </span>
          </div>
          <div
            v-else
            class="unknown-command-message"
          >
            Unknown command
          </div>
          <div class="operation-link">
            <router-link
              v-if="!actionResultIsMissing"
              :to="actionRoute"
              class="operation-name"
              @click="storeOperation(namespace, operation)"
            >
              {{ operation.name }}
            </router-link>
            <span
              v-if="actionResultIsMissing"
              class="operation-name"
            >{{ operation.name }}</span>

            <a
              v-if="!actionResultIsMissing"
              class="toggle-expand-row-button"
            >
              <ChevronDownIcon v-if="!expandedRowIsVisible" />
              <ChevronUpIcon v-if="expandedRowIsVisible" />
            </a>
          </div>
        </div>

        <div class="operation-info-row">
          <div>
            <span
              class="operation-stage"
              :style="{ color: stageColor }"
            >
              <span class="operation-stage-name">{{ stageName }}</span>
              <span
                v-if="commandFailed"
                class="exit-code"
              >
                (exit code {{ exitCode }})
              </span>
            </span>
            <span v-if="workerCompletedTimestamp">
              <DateTime :milliseconds="workerCompletedTimestamp" />
              <span v-if="workerName">
                on
                <router-link
                  class="worker-name"
                  :to="workerRoute"
                  @click="storeOperation(namespace, operation)"
                >{{ workerName }}</router-link>
              </span>
              <span
                v-if="queuedToCompletedTimeDelta"
                class="execution-time"
              > (took {{ formattedTotalExecutionTime(queuedToCompletedTimeDelta) }})</span>
            </span>
          </div>
          <ExecutionMetadata
            :action="operation.action"
            :operation="operation"
          />
        </div>
      </div>
    </div>

    <div
      v-if="expandedRowIsVisible"
      class="expanded-operation-info"
    >
      <MultiColumn>
        <div class="column-wide">
          <ClientIdentityPills
            v-if="clientIdentity"
            :identity="clientIdentity"
          />
          <LogViewer
            :command="operation.command"
            :result="operation.response ? operation.response.result : undefined"
            :update-url="false"
          />
          <CommandMetadata
            v-if="operation.command"
            :command="operation.command"
          />
          <ClientRequestMetadata
            v-if="requestMetadata"
            :request-metadata="requestMetadata"
          />
        </div>
        <div class="column sticky">
          <div v-if="operation">
            <OperationTimeline :operation="operation" />
          </div>
        </div>

      </MultiColumn>
      <div class="collapse-expanded-row-bottom-button">
        <a
          class="toggle-expand-row-button"
          @click="toggleExpandedRow()"
        >
          <ChevronUpIcon />
        </a>
      </div>
    </div>
  </div>
</template>

<script lang="ts">
import { ChevronDownIcon, ChevronUpIcon } from '@heroicons/vue/outline'
import { mapActions } from 'pinia'

import ClientIdentityPills from '@/components/ClientIdentityPills.vue'
import ClientRequestMetadata from '@/components/ClientRequestMetadata.vue'
import CommandMetadata from '@/components/CommandMetadata.vue'
import DateTime from '@/components/DateTime.vue'
import ExecutionMetadata from '@/components/ExecutionMetadata.vue'
import LogViewer from '@/components/LogViewer.vue'
import MultiColumn from '@/components/MultiColumn.vue'
import OperationTimeline from '@/components/OperationTimeline.vue'
import SectionHeading from '@/components/SectionHeading.vue'

import operations, { DecodedOperation } from '@/apis/operations'
import { RequestMetadata } from '@/protos/build/bazel/remote/execution/v2/remote_execution'
import { ClientIdentity } from '@/protos/buildgrid/v2/identity'
import { Timestamp } from '@/protos/google/protobuf/timestamp'
import { useOperationsCache } from '@/stores/operations'
import { defineComponent, PropType } from 'vue'

export default defineComponent({
  name: 'OperationItem',
  components: {
    ChevronDownIcon,
    ChevronUpIcon,
    ClientIdentityPills,
    ClientRequestMetadata,
    CommandMetadata,
    DateTime,
    ExecutionMetadata,
    LogViewer,
    MultiColumn,
    OperationTimeline,
    SectionHeading
  },
  props: {
    operation: {
      type: Object as PropType<DecodedOperation>,
      default: () => null
    }
  },
  data() {
    return {
      clientIdentity: undefined as ClientIdentity | undefined,
      expandedRowIsVisible: false,
      requestMetadata: undefined as RequestMetadata | undefined
    }
  },
  computed: {
    namespace() {
      return (this.$route.params.namespace || '') as string
    },
    stageName() {
      return operations.stageName(this.operation)
    },
    workerName() {
      return this.operation.response?.result?.executionMetadata?.worker
    },
    workerCompletedTimestamp() {
      const ts = this.operation.response?.result?.executionMetadata?.workerCompletedTimestamp
      return ts ? this.timestampToMilliseconds(ts) : undefined
    },
    queuedToCompletedTimeDelta() {
      const executionMetadata = this.operation.response?.result?.executionMetadata
      if (executionMetadata?.queuedTimestamp && executionMetadata?.workerCompletedTimestamp) {
        const start = this.timestampToMilliseconds(executionMetadata.queuedTimestamp)
        const end = this.timestampToMilliseconds(executionMetadata.workerCompletedTimestamp)
        const deltaSecs = (end - start) / 1000
        return deltaSecs
      }
      return undefined
    },
    operationWasCancelled() {
      return operations.operationWasCancelled(this.operation)
    },
    exitCode() {
      return operations.exitCode(this.operation)
    },
    commandFailed() {
      return operations.commandFailed(this.operation)
    },
    actionResultIsMissing() {
      return operations.operationResultIsMissing(this.operation)
    },
    actionRoute() {
      return {
        name: 'Action',
        params: {
          namespace: this.namespace,
          hash: this.operation?.metadata?.actionDigest?.hash || '',
          sizeBytes: (this.operation?.metadata?.actionDigest?.sizeBytes || '').toString()
        },
        query: {
          operation: this.operation.name
        }
      }
    },
    workerRoute() {
      return {
        name: 'Worker Invocations',
        params: {
          namespace: this.namespace,
          workerName: this.workerName
        }
      }
    },
    stageColor() {
      return operations.stageColor(this.operation)
    },
    stageIcon() {
      return operations.stageIcon(this.operation)
    }
  },
  methods: {
    ...mapActions(useOperationsCache, ['storeOperation']),
    toggleExpandedRow() {
      if (!this.actionResultIsMissing) {
        this.expandedRowIsVisible = !this.expandedRowIsVisible
      }
      if (!this.clientIdentity) {
        operations.getClientIdentity(this.namespace, this.operation.name).then(r => this.clientIdentity = r)
      }
      if (!this.requestMetadata) {
        operations.getRequestMetadata(this.namespace, this.operation.name).then(r => this.requestMetadata = r)
      }
    },
    timestampToMilliseconds(timestamp: Timestamp) {
      return (Number(timestamp.seconds) * 1000) + (timestamp.nanos * 0.000001)
    },
    formattedTotalExecutionTime(numSecs: number) {
      if (numSecs) {
        return numSecs.toFixed(1) + ' s'
      }
      return ''
    }
  }
})
</script>

<style lang="scss" scoped>
.operation {
  flex: 1;
  margin: 2rem 0;

  &:first-of-type {
    margin-top: 0;
  }

  .operations-summary {
    display: flex;
    align-items: center;
    padding-bottom: 0.5em;
  }

  .operation-summary-info {
    width: 100%;
    padding-top: 0.5em;
  }

  .operations-summary .stage-icon {
    flex: 0 0 2rem;
    margin-right: 1rem;
  }

  .operation-info-row {
    display: flex;
    justify-content: space-between;

    .command-listing {
      flex: 1 0 auto;
    }

    .operation-link {
      flex: 0 0 auto;
    }
  }

  .operation-stage {
    margin-right: 0.5em;
  }

  .operation-stage-name {
    font-weight: 900;
  }

  .command-listing {
    font-family: 'Roboto Mono';
    margin-bottom: 0.5em;
    max-width: 45rem;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;

    &>.command-argument {
      margin-right: 0.5rem;

      &:first-child {
        font-weight: 900;
      }
    }
  }

  .unknown-command-message {
    color: var(--color-text-muted);
    margin-bottom: 0.5em;
  }

  .expanded-operation-info {
    margin: 0.5rem 0 2rem 1rem;
    padding-left: 2rem;
    border-left: 2px solid var(--color-timeline-line);
  }

  .operation-name {
    font-family: 'Roboto Mono';
    font-size: 0.75rem;
    color: var(--color-link);
    text-decoration: none;

    &:hover {
      box-shadow: 0 -2px 0 var(--color-link) inset;
    }
  }

  .worker-name {
    font-family: 'Roboto Mono';
    font-size: 80%;
    color: var(--color-link);
    text-decoration: none;

    &:hover {
      box-shadow: 0 -2px 0 var(--color-link) inset;
    }
  }

  .exit-code {
    font-size: 0.9rem;
    color: var(--color-red);
  }

  .execution-time {
    font-size: 0.8rem;
  }

  .toggle-expand-row-button {
    color: var(--color-title);
    margin-left: 1em;

    cursor: pointer;

    svg {
      width: 1em;
      height: 1em;
    }
  }

  .collapse-expanded-row-bottom-button {
    text-align: center;

    cursor: pointer;

    svg {
      width: 1em;
      height: 1em;
    }
  }

  :deep(.split-pill span),
  :deep(.metadata .pill) {
    padding: 0.25rem 0.5rem;
  }
}
</style>
