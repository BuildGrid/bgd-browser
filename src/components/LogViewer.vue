<!--
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<template>
  <div
    class="log"
    :class="{ 'popout': popout }"
  >
    <div class="log-wrapper">
      <div class="flex-row">
        <p
          v-if="command"
          class="command"
        >
          <span class="prompt">
            {{ command.workingDirectory || '.' }} $
          </span>
          <span
            v-for="word in command.arguments"
            :key="word"
            class="command-word"
          >
            {{ word }}{{ ' ' }}
          </span>
        </p>
        <div>
          <div
            v-if="liveStdout || liveStderr"
            class="control-group live"
          >
            <span class="dot" />
            Live
          </div>
        </div>
      </div>
      <div
        v-if="downloadableStdout || downloadableStderr"
        class="control-group"
      >
        <div>
          Download:
          <a
            v-if="result?.stdoutDigest"
            class="btn"
            :href="stdoutUrl"
          >stdout</a>
          <a
            v-if="result?.stderrDigest"
            class="btn"
            :href="stderrUrl"
          >stderr</a>
        </div>
      </div>
      <div
        v-if="stdout"
        class="log-content"
      >
        <h2>
          <a
            class="btn"
            :class="{ 'active': showStdout }"
            @click="toggleStdout"
          >
            <ChevronRightIcon
              v-if="!showStdout"
              class="icon"
            />
            <ChevronDownIcon
              v-if="showStdout"
              class="icon"
            />
            Standard Output
          </a>
        </h2>
        <div v-if="showStdout">
          <div
            v-for="(line, i) in stdoutLines"
            :key="line"
            class="stdout-line"
          >
            <span class="line-number">
              {{ i + 1 }}
            </span>
            <span
              class="line-content"
              v-html="line"
            />
          </div>
        </div>
      </div>
      <div
        v-if="stderr"
        class="log-content"
      >
        <h2>
          <a
            class="btn"
            :class="{ 'active': showStderr }"
            @click="toggleStderr"
          >
            <ChevronRightIcon
              v-if="!showStderr"
              class="icon"
            />
            <ChevronDownIcon
              v-if="showStderr"
              class="icon"
            />
            Standard Error
          </a>
        </h2>
        <div v-if="showStderr">
          <div
            v-for="(line, i) in stderrLines"
            :key="line"
            class="stderr-line"
          >
            <span class="line-number">
              {{ i + 1 }}
            </span>
            <span
              class="line-content"
              v-html="line"
            />
          </div>
        </div>
      </div>
      <div v-if="!liveStdout && !liveStderr && !resolvedResult && completed">
        <div class="log-content">
          <h2>ActionResult not found.</h2>
        </div>
      </div>
      <div v-if="!liveStdout && !liveStderr && !resolvedResult && !completed">
        <div class="log-content">
          <h2>Action currently executing, live logs unavailable.</h2>
        </div>
      </div>
    </div>


    <button
      v-if="!popout"
      @click="showModal"
    >
      <ArrowsExpandIcon />
    </button>
    <button
      v-if="popout"
      @click="hideModal"
    >
      <XIcon />
    </button>
  </div>

  <div
    v-if="popout"
    class="backdrop"
    @click="hideModal"
  />
</template>

<script lang="ts">
import { defineComponent, PropType } from 'vue';

import { ArrowsExpandIcon, XIcon } from "@heroicons/vue/outline";
import { ChevronDownIcon, ChevronRightIcon } from '@heroicons/vue/solid';

import { DecodedActionResult } from '@/apis/actions';
import { getConfig } from '@/apis/base';
import bytestream from '@/apis/bytestream';
import { DecodedOperation } from '@/apis/operations';
import { Command, ExecutionStage_Value } from '@/protos/build/bazel/remote/execution/v2/remote_execution';
import { ReadRequest } from '@/protos/google/bytestream/bytestream';
import Convert from 'ansi-to-html';

const convert = new Convert()

// Upper limit on the size of the content that can be rendered.
// Logs larger than this size will be linked to a downloadable URL.
const MAX_LOG_SIZE_BYTES = 1024 * 1024

export default defineComponent({
  name: 'LogViewer',
  components: {
    ArrowsExpandIcon,
    ChevronDownIcon,
    ChevronRightIcon,
    XIcon
  },
  props: {
    command: {
      type: Object as PropType<Command | null | undefined>,
      default: () => null
    },
    operation: {
      type: Object as PropType<DecodedOperation | null | undefined>,
      default: () => null
    },
    result: {
      type: Object as PropType<DecodedActionResult | null | undefined>,
      default: () => null
    },
    updateUrl: {
      type: Boolean,
      default: true
    }
  },
  data() {
    return {
      handledResult: false,
      liveStderr: false,
      liveStdout: false,
      popout: false,
      showStderr: false,
      showStdout: false,
      stderr: '',
      stdout: '',
      wsStderr: null as WebSocket | null,
      wsStdout: null as WebSocket | null
    }
  },
  computed: {
    namespace() {
      return (this.$route.params.namespace || '') as string
    },
    resolvedResult() {
      if (this.result) {
        return this.result
      } else if (this.operation?.response?.result) {
        return this.operation?.response?.result
      }
      return null
    },
    stderrLines() {
      return this.stderr.split(/\r\n|\r|\n/).map(line => convert.toHtml(line))
    },
    stdoutLines() {
      return this.stdout.split(/\r\n|\r|\n/).map(line => convert.toHtml(line))
    },
    downloadableStdout() {
      return this.resolvedResult?.stdoutDigest && this.resolvedResult?.stdoutDigest.sizeBytes > MAX_LOG_SIZE_BYTES
    },
    downloadableStderr() {
      return this.resolvedResult?.stderrDigest && this.resolvedResult?.stderrDigest.sizeBytes > MAX_LOG_SIZE_BYTES
    },
    stdoutUrl() {
      return this.resolvedResult?.stdoutDigest
        ? bytestream.blobUrl(this.namespace, this.resolvedResult?.stdoutDigest)
        : ''
    },
    stderrUrl() {
      return this.resolvedResult?.stderrDigest
        ? bytestream.blobUrl(this.namespace, this.resolvedResult?.stderrDigest)
        : ''
    },
    completed() {
      return !!this.resolvedResult || !this.operation || this.operation.metadata?.stage === ExecutionStage_Value.COMPLETED
    },
    stdoutStreamName() {
      return this.operation?.metadata?.stdoutStreamName
    },
    stderrStreamName() {
      return this.operation?.metadata?.stderrStreamName
    },
  },
  watch: {
    operation() {
      this.populateLogs()
    },
    result() {
      if (this.result) {
        this.getLogsFromResult(this.result)
      }
    }
  },
  created() {
    this.populateLogs()

    this.showStderr = !!this.$route.query.show_stderr
    this.showStdout = !!this.$route.query.show_stdout

    // TODO: switch to Composition API so that calling the method like this
    // doesn't break the linter. This approach to calling does work, despite
    // it looking like we'll lose the scope to the linter.
    window.addEventListener('keyup', this.handleKeyPress) // eslint-disable-line @typescript-eslint/unbound-method
  },
  unmounted() {
    window.removeEventListener('keyup', this.handleKeyPress) // eslint-disable-line @typescript-eslint/unbound-method
  },
  methods: {
    _initLogStreamWebsocket(request: ReadRequest) {
      const config = getConfig(this.namespace)
      const ws = new WebSocket(config.logStreamUrl)
      ws.onmessage = e => this._receiveLogStream(e)
      ws.onopen = () => {
        ws?.send(ReadRequest.toBinary(request))
      }
      return ws
    },
    _receiveLogStream(event: MessageEvent<any>) {
      let message: { resource_name?: string, complete?: boolean, data?: string } = {}
      try {
        // eslint-disable-next-line
        message = JSON.parse(event.data)
      } catch (e) {
        console.warn(`Unexpected error receiving LogStream message: ${e}`)
        return
      }

      if (message.resource_name == this.stdoutStreamName) {
        if (message.complete) {
          this.liveStdout = false
        } else if (message.data) {
          this.stdout += message.data
        }
      } else if (message.resource_name == this.stderrStreamName) {
        if (message.complete) {
          this.liveStderr = false
        } else if (message.data) {
          this.stderr += message.data
        }
      } else {
        console.warn(`Received message for unknown LogStream: ${message.resource_name}`)
      }
    },
    async getLogsFromResult(result?: DecodedActionResult) {
      if (!result) {
        return
      }

      this.stderr = atob(result.stderrRaw)
      this.stdout = atob(result.stdoutRaw)

      if (result.stderrDigest && !result.stderrRaw) {
        const digest = result.stderrDigest
        this.stderr = await bytestream.readBlob(this.namespace, digest.hash, digest.sizeBytes)
      }
      if (result.stdoutDigest && !result.stdoutRaw) {
        const digest = result.stdoutDigest
        this.stdout = await bytestream.readBlob(this.namespace, digest.hash, digest.sizeBytes)
      }

      this.handledResult = true
    },
    getLogStream() {
      if (this.operation && !this.operation.done) {
        if (this.operation.metadata?.stdoutStreamName && this.wsStdout === null) {
          const stdoutRequest = ReadRequest.create()
          stdoutRequest.resourceName = this.operation.metadata?.stdoutStreamName
          const config = getConfig(this.namespace)
          try {
            this.wsStdout = this._initLogStreamWebsocket(stdoutRequest)
            this.liveStdout = true
          } catch {
            console.error(
              `Failed to open WebSocket connection to ${config.logStreamUrl} for LogStream ${stdoutRequest.resourceName}`
            )
            this.liveStdout = false
          }
        }

        // Set up a separate websocket for streaming stderr. When the backend can
        // handle multiplexing requests in a single socket this can be removed in
        // favour of sending both requests to a single socket.
        if (this.operation.metadata?.stderrStreamName && this.wsStderr === null) {
          const config = getConfig(this.namespace)
          const stderrRequest = ReadRequest.create()
          stderrRequest.resourceName = this.operation.metadata?.stderrStreamName
          try {
            this.wsStderr = this._initLogStreamWebsocket(stderrRequest)
            this.liveStderr = true
          } catch {
            console.error(
              `Failed to open WebSocket connection to ${config.logStreamUrl} for LogStream ${stderrRequest.resourceName}`
            )
            this.liveStderr = false
          }
        }
      }
    },
    populateLogs() {
      // If there is a result available, use the logs from the result.
      // Otherwise, attempt to connect to a LogStream for live log population.
      if (this.result || this.operation?.response?.result) {
        this.getLogsFromResult(this.result || this.operation?.response?.result)
      } else {
        this.getLogStream()
      }
    },
    handleKeyPress(event: KeyboardEvent) {
      if (event.key === "Escape") {
        this.hideModal()
      }
    },
    hideModal() {
      this.popout = false
    },
    showModal() {
      this.popout = true
    },
    toggleStdout() {
      this.showStdout = !this.showStdout
      if (this.updateUrl) {
        this.$router.replace({ query: { ...this.$route.query, show_stdout: this.showStdout ? `${this.showStdout}` : undefined } })
      }
    },
    toggleStderr() {
      this.showStderr = !this.showStderr
      if (this.updateUrl) {
        this.$router.replace({ query: { ...this.$route.query, show_stderr: this.showStderr ? `${this.showStderr}` : undefined } })
      }
    }
  }
})
</script>

<style lang="scss" scoped>
.log {
  font-family: 'Roboto Mono';
  background-color: var(--color-code-bg);
  padding: 1.25rem;
  border-radius: 0.5rem;
  color: #fff;
  font-size: 1rem;
  box-shadow: 0 -0.2rem .4rem rgba(0, 0, 0, .1) inset;
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;

  .log-wrapper {
    flex: 1;
  }

  +.backdrop {
    display: none;
  }

  &.popout {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    margin: 1rem;
    z-index: 20;
    overflow: auto;
    box-shadow: 0 .2rem .4rem rgba(0, 0, 0, .1);

    .flex-row {
      margin: 0;
    }

    +.backdrop {
      display: block;
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      z-index: 10;
      background-color: rgba(0, 0, 0, .5);
    }
  }

  .flex-row {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;

    &:last-child {
      margin-bottom: 0;
    }
  }

  .control-group {
    a {
      cursor: pointer;
      transition: 150ms ease-in-out all;

      &.btn {
        padding: 10px;
        color: #7BB2D9;

        &.active {
          color: #B9CDDA;
          box-shadow: 0 -4px 0 #B9CDDA inset;

          &:hover {
            box-shadow: 0 -2px 0 #B9CDDA inset;
          }
        }

        &:hover {
          color: #B9CDDA;
        }
      }
    }

    &.live {
      color: #B9CDDA;
      display: flex;
      align-items: center;

      .dot {
        height: 10px;
        width: 10px;
        margin-right: 15px;
        background-color: #f24949;
        border-radius: 50%;
        display: inline-block;
        box-shadow: 0 0 5px #f24949;
      }
    }
  }

  .command {
    margin: 0;
    flex: 1 0 0;
    word-break: break-word;

    .prompt {
      color: #aaa;
      margin-right: 0.5rem;
    }

    .command-word:first-of-type {
      font-weight: 500;
    }
  }

  .log-content {
    margin-top: 2rem;
    word-break: break-word;

    h2 {
      font-size: 0.9rem;
      color: #aaa;
      margin: 0 0 0.5rem 0;
    }

    .icon {
      width: 1.25rem;
      height: 1.25rem;
      margin-right: 0.5rem;
      color: var(--color-blue);
    }

    a {
      display: flex;
      cursor: pointer;
      color: var(--color-blue);

      &:hover,
      &:hover .icon {
        color: var(--color-blue-light);
      }
    }

    span {
      font-size: 1rem;
      margin: 0;
      white-space: pre-wrap;
    }

    .stdout-line,
    .stderr-line {
      display: flex;

      span {
        display: inline-block;
      }

      .line-number {
        color: #9bb0d9;
        text-align: right;
        flex: 0 0 3rem;
        margin-right: 1rem;
        padding-right: 0.5rem;
        border-right: 1px #9bb0d9 solid;
        user-select: none;
        -webkit-user-select: none;

        &:hover {
          box-shadow: -2px 0 #9bb0d9 inset;
        }
      }

      &:hover {
        background-color: var(--color-timeline-line);
      }
    }
  }
}

button {
  display: flex;
  position: sticky;
  top: 0;
  right: 0;
  padding: 0.5rem;
  border-radius: 0.5rem;
  background-color: var(--color-grey-dark);
  transition: 100ms ease-in-out all;
  color: var(--color-blue);

  &:hover {
    background-color: var(--color-grey);
    color: var(--color-blue-light);
  }
}

svg {
  width: 1.5rem;
  height: 1.5rem;
}
</style>
