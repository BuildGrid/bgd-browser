<!--
  Copyright 2021 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<template>
  <div v-if="metadata">
    <div class="timeline">
      <div
        v-if="validTimestamp(queuedTimestamp)"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Action added to queue
          </p>
          <p class="timestamp">
            <DateTime :milliseconds="queuedTimestamp" />
          </p>
        </div>
      </div>
      <div
        v-if="metadata?.worker"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Assigned to worker
            <router-link
              class="worker-name"
              :to="workerRoute"
            >
              {{ workerName }}
            </router-link>
          </p>
          <p v-if="validTimestamp(workerStartedTimestamp)" class="timestamp">
            <DateTime :milliseconds="workerStartedTimestamp" />
          </p>
        </div>
        <div
          v-if="validTimestamp(queuedTimestamp)"
          class="timeline-entry-difference"
        >
          +
          <span class="text-lg">
            {{ timestampDiff(queuedTimestamp, workerStartedTimestamp) }}
          </span>
        </div>
      </div>
      <div
        v-if="validTimestamp(inputFetchStartedTimestamp)"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Started fetching inputs
          </p>
          <p class="timestamp">
            <DateTime :milliseconds="inputFetchStartedTimestamp" />
          </p>
        </div>
        <div
          v-if="validTimestamp(workerStartedTimestamp)"
          class="timeline-entry-difference"
        >
          +
          <span class="text-lg">
            {{ timestampDiff(workerStartedTimestamp, inputFetchStartedTimestamp) }}
          </span>
        </div>
      </div>
      <div
        v-if="validTimestamp(inputFetchCompletedTimestamp)"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Completed fetching inputs
          </p>
          <p class="timestamp">
            <DateTime :milliseconds="inputFetchCompletedTimestamp" />
          </p>
        </div>
        <div
          v-if="validTimestamp(inputFetchStartedTimestamp)"
          class="timeline-entry-difference"
        >
          +
          <span class="text-lg">
            {{ timestampDiff(inputFetchStartedTimestamp, inputFetchCompletedTimestamp) }}
          </span>
        </div>
      </div>
      <div
        v-if="validTimestamp(executionStartedTimestamp)"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Started execution
          </p>
          <p class="timestamp">
            <DateTime :milliseconds="executionStartedTimestamp" />
          </p>
        </div>
        <div
          v-if="validTimestamp(inputFetchCompletedTimestamp)"
          class="timeline-entry-difference"
        >
          +
          <span class="text-lg">
            {{ timestampDiff(inputFetchCompletedTimestamp, executionStartedTimestamp) }}
          </span>
        </div>
      </div>
      <div
        v-if="validTimestamp(executionCompletedTimestamp)"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Completed execution
          </p>
          <p class="timestamp">
            <DateTime :milliseconds="executionCompletedTimestamp" />
          </p>
        </div>
        <div
          v-if="validTimestamp(executionStartedTimestamp)"
          class="timeline-entry-difference"
        >
          +
          <span class="text-lg">
            {{ timestampDiff(executionStartedTimestamp, executionCompletedTimestamp) }}
          </span>
        </div>
      </div>
      <div
        v-if="validTimestamp(uploadStartedTimestamp)"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Output upload started
          </p>
          <p class="timestamp">
            <DateTime :milliseconds="uploadStartedTimestamp" />
          </p>
        </div>
        <div
          v-if="validTimestamp(executionCompletedTimestamp)"
          class="timeline-entry-difference"
        >
          +
          <span class="text-lg">
            {{ timestampDiff(executionCompletedTimestamp, uploadStartedTimestamp) }}
          </span>
        </div>
      </div>
      <div
        v-if="validTimestamp(uploadCompletedTimestamp)"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Output upload completed
          </p>
          <p class="timestamp">
            <DateTime :milliseconds="uploadCompletedTimestamp" />
          </p>
        </div>
        <div
          v-if="validTimestamp(uploadStartedTimestamp)"
          class="timeline-entry-difference"
        >
          +
          <span class="text-lg">
            {{ timestampDiff(uploadStartedTimestamp, uploadCompletedTimestamp) }}
          </span>
        </div>
      </div>
      <div
        v-if="validTimestamp(workerCompletedTimestamp)"
        class="timeline-entry"
      >
        <div class="timeline-entry-content">
          <p class="details">
            Worker reported completion
          </p>
          <p class="timestamp">
            <DateTime :milliseconds="workerCompletedTimestamp" />
          </p>
        </div>
        <div
          v-if="validTimestamp(uploadCompletedTimestamp)"
          class="timeline-entry-difference"
        >
          +
          <span class="text-lg">
            {{ timestampDiff(uploadCompletedTimestamp, workerCompletedTimestamp) }}
          </span>
        </div>
      </div>
      <div v-else>
        <div class="timeline-open" />
      </div>
    </div>
  </div>
</template>

<script lang='ts'>
import { defineComponent, PropType } from 'vue'

import { DecodedOperation } from '@/apis/operations'
import DateTime from '@/components/DateTime.vue'
import { Timestamp } from '@/protos/google/protobuf/timestamp'

function timestampToMilliseconds(ts?: Timestamp) {
  let ms = 0
  if (ts) {
    ms = Number(ts.seconds) * 1000
    ms += ts.nanos * 0.000001
  }
  return ms
}

export default defineComponent({
  name: 'OperationTimeline',
  components: {
    DateTime
  },
  props: {
    operation: {
      type: Object as PropType<DecodedOperation>,
      default: () => null
    }
  },
  computed: {
    namespace(): string {
      return (this.$route.params.namespace || '') as string
    },
    metadata() {
      if (this.operation?.response?.result) {
        return this.operation.response.result.executionMetadata
      } else {
        return this.operation?.metadata?.partialExecutionMetadata
      }
    },
    queuedTimestamp() {
      return timestampToMilliseconds(this.metadata?.queuedTimestamp)
    },
    executionStartedTimestamp() {
      return timestampToMilliseconds(this.metadata?.executionStartTimestamp)
    },
    executionCompletedTimestamp() {
      return timestampToMilliseconds(this.metadata?.executionCompletedTimestamp)
    },
    inputFetchStartedTimestamp() {
      return timestampToMilliseconds(this.metadata?.inputFetchStartTimestamp)
    },
    inputFetchCompletedTimestamp() {
      return timestampToMilliseconds(this.metadata?.inputFetchCompletedTimestamp)
    },
    uploadStartedTimestamp() {
      return timestampToMilliseconds(this.metadata?.outputUploadStartTimestamp)
    },
    uploadCompletedTimestamp() {
      return timestampToMilliseconds(this.metadata?.outputUploadCompletedTimestamp)
    },
    workerStartedTimestamp() {
      return timestampToMilliseconds(this.metadata?.workerStartTimestamp)
    },
    workerCompletedTimestamp() {
      return timestampToMilliseconds(this.metadata?.workerCompletedTimestamp)
    },
    workerName() {
      return this.metadata?.worker || 'unknown'
    },
    workerRoute() {
      return {
        name: 'Worker Invocations',
        params: {
          namespace: this.namespace,
          workerName: this.workerName
        }
      }
    }
  },
  methods: {
    timestampDiff(start: number, end: number) {
      // NOTE: Not using date-fns to format the difference here because
      // date-fns Duration objects don't keep milliseconds, and often
      // this diff will be < 1 second.
      const MS_IN_HOURS = 1000 * 60 * 60
      const MS_IN_MINUTES = 1000 * 60
      const MS_IN_SECONDS = 1000

      let timeDiff = end - start
      const hours = Math.floor(timeDiff / MS_IN_HOURS)
      timeDiff %= MS_IN_HOURS

      const minutes = Math.floor(timeDiff / MS_IN_MINUTES)
      timeDiff %= MS_IN_MINUTES

      const seconds = Math.floor(timeDiff / MS_IN_SECONDS)
      timeDiff %= MS_IN_SECONDS

      const miliseconds = Math.floor(timeDiff)

      return `${hours > 0 ? hours + 'h' : ''}
              ${minutes > 0 ? minutes + 'm' : ''}
              ${seconds > 0 ? seconds + 's' : ''}
              ${miliseconds >= 0 ? miliseconds + 'ms' : ''}`
    },
    validTimestamp(timestamp: number) {
      // Only times later than the Unix epoch are valid.
      //
      // This ensures we don't try to render timestamps where the value
      // is defaulted in the gRPC message.
      return timestamp > 0
    }
  },
})
</script>

<style lang="scss" scoped>
.timeline {
  position: relative;
  padding-left: 3rem;

  &::before {
    content: "";
    width: 2px;
    top: 1.75rem;
    bottom: 3.5rem;
    left: 1.5rem;
    position: absolute;
    background: var(--color-timeline-line);
  }

  .timeline-entry {
    padding: 1rem 0;
    position: relative;
    display: flex;
    justify-content: space-between;

    &::before {
      content: "";
      width: 0.5rem;
      height: 0.5rem;
      background: var(--color-timeline-entry);
      border-radius: 20px;
      left: -1.8125rem;
      top: calc(50% - 20px);
      position: absolute;
      border: 0.125rem var(--color-bg) solid;
    }

    .details {
      margin: 0;
    }

    .timestamp {
      margin: 0.25rem 0;
      color: var(--color-text-muted);
      font-size: 0.9rem;
    }

    a {
      color: var(--color-link);
      text-decoration: none;
      transition: all 100ms ease-in-out;

      &:hover {
        color: var(--color-link-hover);
        box-shadow: 0 -2px 0 var(--color-link-hover) inset;
      }
    }
  }

  .timeline-open {
    position: relative;
    padding: 30px 0;
  }
}
</style>
