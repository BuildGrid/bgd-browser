<!--
  Copyright 2023 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<script setup lang="ts">
import { SearchIcon } from "@heroicons/vue/solid";
import { computed, defineEmits, defineProps, onMounted, ref } from "vue";
import { useRoute } from "vue-router";

import operations, { OperationFilter } from "@/apis/operations.ts";

interface FilterOption {
  description?: string;
  key: string;
  name: string;
}

interface Props {
  defaultText: string;
  modelValue: string;
  prompt?: string;
}

const props = defineProps<Props>()

const emit = defineEmits(["input", "update:modelValue"])

const route = useRoute()

const cursorPosition = ref<number | null>(null)
const focused = ref(false)
const inputElement = ref<HTMLInputElement | null>(null)

const namespace = (route.params.namespace || "") as string
let filterSpecs: OperationFilter[] = []

const query = computed({
  get() {
    return props.modelValue;
  },
  set(value: string) {
    emit("update:modelValue", value)
    emit("input")
  },
})

const filters = computed(() => {
  return query.value.split("&").map((filter, index, filters) => {
    // Determine the bounds of this filter within the full string
    const start = filters.slice(0, index).reduce((acc, cur) => {
      return acc + cur.length + 1;
    }, 0)
    const end = start + filter.length
    const active =
      cursorPosition.value !== null && start <= cursorPosition.value && cursorPosition.value <= end

    // Extract the key, comparator, and value
    const re = /(?<key>[a-zA-Z\-_]+) ?(?<comparator>[!=<>]+)? ?(?<value>[0-9a-zA-Z-:.+()_]+)?/d
    const match: RegExpMatchArray | null = filter.match(re)

    return {
      active: active,
      end: end,
      start: start,
      key: {
        bounds: match?.indices?.groups?.key,
        content: match?.groups?.key,
      },
      comparator: {
        bounds: match?.indices?.groups?.comparator,
        content: match?.groups?.comparator,
      },
      value: {
        bounds: match?.indices?.groups?.value,
        content: match?.groups?.value,
      },
    }
  })
})

const activeFilter = computed(() => {
  return filters.value.filter((filter) => filter.active).pop();
})

const activeFragment = computed(() => {
  const within = (interval: [number, number] | undefined, offset: number = 0) => {
    return (
      !!interval &&
      cursorPosition.value !== null &&
      cursorPosition.value >= interval[0] + offset &&
      cursorPosition.value <= interval[1] + offset
    )
  }

  if (!activeFilter.value) {
    return {
      type: "key",
      bounds: [0, 0],
      content: "",
    }
  }

  const { start, end, key, comparator, value } = activeFilter.value

  if (activeFilter.value) {
    const currentSpec = filterSpecs
      .filter((spec) => spec.key === activeFilter.value?.key.content)
      .pop()
    if (
      within(key.bounds, start) ||
      !currentSpec ||
      (cursorPosition.value === key.bounds[1] && !!comparator.bounds)
    ) {
      return {
        type: "key",
        ...key,
      }
    } else if (
      within(comparator.bounds, start) ||
      (cursorPosition.value == end && !comparator.bounds)
    ) {
      return {
        type: "comparator",
        ...comparator,
      }
    } else if (within(value.bounds, start) || (cursorPosition.value == end && !value.bounds)) {
      return {
        type: "value",
        ...value,
      }
    } else {
      return null
    }
  } else {
    return null
  }
})

const dropdownContent = computed(() => {
  if (!filterSpecs.length) {
    return null
  }

  let content = ""
  let title = "Separator"
  let options = []

  const currentSpec = filterSpecs
    .filter((spec) => spec.key === activeFilter.value?.key.content)
    .pop()

  switch (activeFragment.value?.type) {
    case "key":
      title = "Filter parameters"
      options = filterSpecs.filter((spec) => spec.key.match(activeFragment.value?.content))
      if (!options.length) {
        content = "No valid matching filter parameters."
      }
      break
    case "comparator":
      title = "Comparator"
      options = currentSpec.comparators.map((comparator) => {
        return {
          key: comparator,
          name: comparator,
        }
      })
      break;
    case "value":
      title = "Value"
      if (currentSpec.values) {
        options = currentSpec.values?.map((value) => {
          const name = value
            .replace("_", " ")
            .split(" ")
            .map((word) => word.slice(0, 1) + word.slice(1).toLowerCase())
            .join(" ")
          return {
            key: value.toLowerCase(),
            name,
          }
        })
      } else {
        content = "This is a free-form field, type something to search for in it"
      }
      break
    default:
      title = "Separator"
      options = [
        {
          key: "&",
          name: "&",
          description: "Add another filter combined using AND",
        },
      ]
      break
  }
  return {
    content,
    options,
    title,
  }
})

const blur = (e: Event) => {
  if (!e.relatedTarget) {
    focused.value = false;
  }
}
const focus = () => {
  focused.value = true;
  inputElement.value?.focus();
}
const updateCursorPosition = (event: Event) => {
  cursorPosition.value = (event.target as HTMLInputElement).selectionStart
}
const updateQuery = (option: FilterOption) => {
  if (!query.value || !activeFragment.value?.bounds) {
    query.value += option.key + " "
  } else {
    const start = activeFilter.value.start + activeFragment.value.bounds[0]
    const end = start + activeFragment.value.content.length
    const left = query.value.slice(0, start).trim()
    const right = query.value.slice(end).trim()
    query.value = left + `${left ? " " : ""}` + option.key + " " + right
  }
}

onMounted(() => {
  operations.getFilters(namespace).then(({ data }) => (filterSpecs = data))
})
</script>

<template>
  <div class="search-input">
    <div
      class="filter-container"
      :class="{ focused }"
      tabindex="0"
      @focus.capture="focus"
      @blur.capture="blur"
    >
      <SearchIcon class="muted" />
      <span
        v-if="!!props.prompt"
        class="prompt"
      >
        {{ props.prompt }}
      </span>
      <div class="group">
        <input
          ref="inputElement"
          v-model="query"
          type="text"
          @click="updateCursorPosition"
          @selectionchange="updateCursorPosition"
        >
        <span
          v-if="!query"
          class="prompt absolute"
        >
          {{ props.defaultText }}
        </span>
        <div
          v-if="focused && dropdownContent"
          class="dropdown"
        >
          <h2>{{ dropdownContent.title }}</h2>
          <p
            v-if="dropdownContent.content && !dropdownContent.options.length"
            class="content muted"
          >
            {{ dropdownContent.content }}
          </p>
          <div class="filters">
            <a
              v-for="option in dropdownContent.options"
              :key="option.key"
              class="filter-shortcut"
              @click="updateQuery(option)"
            >
              <p class="title">{{ option.name }}</p>
              <p
                v-if="option.description"
                class="description"
              >
                {{ option.description }}
              </p>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<style scoped lang="scss">
.filter-container {
  display: flex;
  align-items: center;
  background-color: var(--color-bg-top);
  font-size: 1.1rem;
  border-radius: 0.5rem;
  padding: 0 0 0 1rem;
  box-shadow: 0 0.2rem 0.4rem rgba(0, 0, 0, 0.1);
  transition: 150ms ease-in-out all;

  svg {
    height: 1.5rem;
    width: 1.5rem;
    margin-right: 1rem;
  }

  &.focused {
    box-shadow: 0 0 0.2rem rgba(0, 0, 0, 0.1), 0 0.2rem 0.4rem rgba(0, 0, 0, 0.3);
  }

  .prompt {
    color: var(--color-neutral);
  }

  input {
    border: none;
    flex: 1 0 auto;
    font-size: 1.1rem;
    padding: 1rem;
    border-radius: 0.5rem;
    background-color: var(--color-bg-top);
    color: var(--color-text);
  }

  .group {
    display: flex;
    position: relative;
    flex: 1 0 auto;

    .absolute {
      position: absolute;
      top: 0;
      padding: 1rem;
    }

    .dropdown {
      position: absolute;
      top: calc(100% + 0.5rem);
      padding: 1.5rem;
      background-color: var(--color-bg-top);
      border-radius: 0.5rem;
      box-shadow: 0 0 0.2rem rgba(0, 0, 0, 0.1);
      font-size: 1rem;
      width: calc(100% - 3rem);
      z-index: 10;

      h2 {
        margin: 0 0 1rem 0;
        padding: 0 0.75rem;
        text-transform: uppercase;
        font-size: 1rem;
        color: var(--color-title);
        letter-spacing: 0.04rem;
      }

      .content {
        padding: 0 0.75rem;
        margin: 0;
      }

      .filters {
        display: grid;
        grid-template-columns: 50% 50%;
        gap: 0.5rem;

        @media (min-width: 1400px) {
          grid-template-columns: 33% 33% 33%;
        }

        .filter-shortcut {
          border-radius: 0.75rem;
          padding: 0.75rem;
          cursor: pointer;
          flex: 1 0 33%;

          &:hover {
            background-color: var(--color-bg);
          }

          p {
            margin: 0;
          }

          .title {
            margin: 0 0 0.25rem 0;
          }

          .description {
            color: var(--color-text-muted);
            font-size: 0.75rem;
          }
        }
      }
    }
  }
}
</style>
