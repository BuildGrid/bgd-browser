/**
  Copyright 2022 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */
// eslint-disable-next-line
import 'highlight.js/styles/stackoverflow-light.css'
import hljs from 'highlight.js/lib/common'
import javascript from 'highlight.js/lib/languages/javascript'
import hljsVuePlugin from '@highlightjs/vue-plugin'
import { createApp } from 'vue'
import { createPinia } from 'pinia'

import { loadConfig } from '@/apis/base'
import { newRouter } from './router'

import App from './App.vue'

async function main() {
  await loadConfig()
  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  const app = createApp(App)
  const pinia = createPinia()

  app.use(newRouter())
  app.use(pinia)
  hljs.registerLanguage('javascript', javascript)
  app.use(hljsVuePlugin)
  app.mount('#app')
}
main()
