/**
  Copyright 2020 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */
import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import indexdb from '@/apis/indexdb'

import { getNamespaces } from '@/apis/base'

export function newRouter() {
  const namespaceBase = getNamespaces() ? '/:namespace' : ''

  let routes: RouteRecordRaw[] = [
    {
      path: `${namespaceBase}/home`,
      name: 'Home',
      component: () => import('../views/Home.vue')
    },
    {
      path: `${namespaceBase}/`,
      name: 'Home',
      component: () => import('../views/Home.vue')
    },
    {
      path: '/history',
      name: 'History',
      component: () => import('../views/History.vue')
    },
    {
      path: `${namespaceBase}/browse`,
      name: 'Browse',
      component: () => import('../views/Browse.vue')
    },
    {
      path: `${namespaceBase}/action/:hash/:sizeBytes`,
      name: 'Action',
      component: () => import('../views/Action.vue')
    },
    {
      path: `${namespaceBase}/directory/:hash/:sizeBytes`,
      name: 'Directory',
      component: () => import('../views/Directory.vue')
    },
    {
      path: `${namespaceBase}/tree/:hash/:sizeBytes`,
      name: 'Tree',
      component: () => import('../views/Tree.vue')
    },
    {
      path: `${namespaceBase}/file/:hash/:sizeBytes`,
      name: 'File',
      component: () => import('../views/File.vue')
    },
    {
      path: `${namespaceBase}/correlated-invocations/:correlationId`,
      name: 'Correlated Invocations',
      component: () => import('../views/CorrelatedInvocations.vue')
    },
    {
      path: `${namespaceBase}/tool-invocations/:toolInvocationId`,
      name: 'Tool Invocation',
      component: () => import('../views/ToolInvocations.vue')
    },
    {
      path: `${namespaceBase}/workers`,
      name: 'Workers',
      component: () => import('../views/Workers.vue')
    },
    {
      path: `${namespaceBase}/worker/:workerName`,
      name: 'Worker Invocations',
      component: () => import('../views/WorkerInvocations.vue')
    },
    {
      path: `${namespaceBase}/operation/:operationName(.*)`,
      name: 'Operation',
      component: () => import('../views/Operation.vue')
    }
  ]
  if (getNamespaces()) {
    routes = routes.concat(
      [
        {
          path: '/home',
          name: 'MultiHome',
          component: () => import('../views/MultiHome.vue')
        },
        {
          path: '/',
          name: 'MultiHome',
          component: () => import('../views/MultiHome.vue')
        }
      ]
    )
  }

  const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
  })

  router.afterEach(async (to) => await indexdb.savePage({
    timestamp: Date.now(),
    url: window.location.href,
    route: {
      name: to.name,
      hash: to.hash,
      params: to.params,
      query: to.query,
      fullPath: to.fullPath
    }
  }))

  return router
}
