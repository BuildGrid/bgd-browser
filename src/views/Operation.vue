<!--
  Copyright 2023 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<script setup lang="ts">
import { computed, onMounted, reactive, watch } from 'vue'
import { useRoute } from 'vue-router'

import bytestream from '@/apis/bytestream'
import operations, { DecodedOperation } from '@/apis/operations'
import Breadcrumbs from '@/components/Breadcrumbs.vue'
import ClientIdentityPills from '@/components/ClientIdentityPills.vue'
import ClientRequestMetadata from '@/components/ClientRequestMetadata.vue'
import CommandMetadata from '@/components/CommandMetadata.vue'
import EmptyView from '@/components/EmptyView.vue'
import ExecutionMetadata from '@/components/ExecutionMetadata.vue'
import ExecutionStats from '@/components/ExecutionStats.vue'
import LayoutWide from '@/components/LayoutWide.vue'
import LogViewer from '@/components/LogViewer.vue'
import MultiColumn from '@/components/MultiColumn.vue'
import OperationTimeline from '@/components/OperationTimeline.vue'
import OperationTreeView from '@/components/OperationTreeView.vue'
import SectionHeading from '@/components/SectionHeading.vue'
import ViewHeading from '@/components/ViewHeading.vue'
import { Directory, RequestMetadata } from '@/protos/build/bazel/remote/execution/v2/remote_execution'
import { ClientIdentity } from '@/protos/buildgrid/v2/identity'
import { useOperationsCache } from '@/stores/operations'
import axios, { AxiosError } from 'axios'

interface State {
  breadcrumbs: Array<{ title: string, target: string }>;
  error?: Error;
  loading: boolean;
  inputRoot?: Directory;
  requestError?: AxiosError;
  requestMetadata?: RequestMetadata;
  clientIdentity?: ClientIdentity;
}

const route = useRoute()
const state: State = reactive({
  breadcrumbs: [{ title: 'Operations', target: '/browse' }],
  error: undefined,
  inputRoot: undefined,
  loading: true,
  requestMetadata: undefined,
  clientIdentity: undefined
})

const namespace = (route.params.namespace || '') as string
const operationName = route.params.operationName as string
const operationsCache = useOperationsCache()

const operation = computed(() => operationsCache.operations.get(operationName))
const auxiliaryMetadataList = computed(() => operation.value?.response?.result?.executionMetadata?.auxiliaryMetadata || [])

function operationCanChangeState(op: DecodedOperation | undefined) {
  return op && !operations.operationIsCompleted(op) && !operations.operationWasCancelled(op)
}

const decodedStatusText = computed(() => state.requestError?.response?.statusText ? decodeURI(state.requestError?.response?.statusText) : '')

// TODO: Once the Action view uses the Composition API this should
// be extracted into a composable for reuse.
let refreshTimer: NodeJS.Timeout | undefined = undefined

function refreshOperation() {
  operationsCache.fetchOperation(namespace, operationName).then(operation => {
    if (!operationCanChangeState(operation) && refreshTimer) {
      clearInterval(refreshTimer)
      console.log(`Auto refresh stopped for ${operationName}`)
    }
  }).catch(e => {
    state.error = e
  })
}

watch(operation, (current, old) => {
  // Theoretical case where the operation has disappeared for some
  // reason. This shouldn't happen under normal operation.
  if (!current) {
    console.log(`Operation ${operationName} became undefined`)
    return
  }

  // If we don't have notification permission, or there is nothing
  // useful to compare against, there's no need to continue
  if (Notification.permission !== 'granted' || !old) {
    return
  }

  const oldStage = operations.stageName(old)
  const newStage = operations.stageName(current)
  if (oldStage === newStage) {
    return
  }

  const notification = new Notification(`Operation ${newStage}`, {
    body: `${current.name}`,
    icon: `${import.meta.env.BASE_URL}bgd-logo-black.png`
  })
  setTimeout(() => notification.close(), 10000)
})

onMounted(() => {
  const operationFetch = operationsCache.fetchOperation(namespace, operationName)
  const metadataFetch = operations.getRequestMetadata(namespace, operationName)
  const identityFetch = operations.getClientIdentity(namespace, operationName)

  Promise.allSettled([operationFetch, metadataFetch, identityFetch]).then(() => {
    state.loading = false
  })

  operationFetch.then(operation => {
    // Once the operation is fetched we need to also try to fetch the input
    // root `Directory` and start up the auto-refreshing for incomplete
    // operations
    if (operation?.action?.inputRootDigest) {
      bytestream.readDirectory(namespace, operation.action.inputRootDigest.hash, operation.action.inputRootDigest.sizeBytes).then(inputRoot => {
        state.inputRoot = inputRoot
      })
    }

    if (operationCanChangeState(operation)) {
      Notification.requestPermission()
      refreshTimer = setInterval(refreshOperation, 5000)
      console.log(`Auto refresh started for ${operationName}`)
    }
  }).catch(e => {
    if (axios.isAxiosError(e)) {
      state.requestError = e
    }
    if (e instanceof Error) {
      state.error = e
    }
  })

  metadataFetch.then(result => {
    state.requestMetadata = result
  }).catch(() => { })

  identityFetch.then(result => { state.clientIdentity = result }).catch(() => { })
})
</script>

<template>
  <LayoutWide v-if="state.loading">
    <EmptyView>
      <template #breadcrumbs>
        <Breadcrumbs :crumbs="state.breadcrumbs" />
      </template>
      <template #heading>
        Operation
      </template>
      Loading...
    </EmptyView>
  </LayoutWide>

  <LayoutWide v-else-if="!state.loading && !operation">
    <EmptyView>
      <template #breadcrumbs>
        <Breadcrumbs :crumbs="state.breadcrumbs" />
      </template>
      <template #heading>
        Operation couldn't be retrieved
      </template>
      <template
        v-if="state.error"
        #default
      >
        {{ state.error.message }}
      </template>
      <template
        v-if="state.requestError?.response"
        #details
      >
        {{ decodedStatusText }}
      </template>
    </EmptyView>
  </LayoutWide>

  <LayoutWide v-else-if="!state.loading && operation">
    <header>
      <Breadcrumbs :crumbs="state.breadcrumbs" />
      <ViewHeading>
        <div class="flex-between">
          <div>
            Operation
            <span class="muted">{{ operation.name }}</span>
          </div>
          <ExecutionMetadata
            :action="operation?.action"
            :operation="operation"
            :response="operation?.response"
          />
        </div>
      </ViewHeading>
      <ClientIdentityPills
        v-if="state.clientIdentity"
        :identity="state.clientIdentity"
      />
    </header>

    <MultiColumn>
      <div class="column-narrow-static sticky">
        <OperationTreeView
          :operation="operation"
          :root="state.inputRoot"
        />
      </div>
      <div class="column-wide">
        <LogViewer
          v-if="operation"
          :command="operation.command"
          :result="operation.response?.result"
          :operation="operation"
        />

        <CommandMetadata
          v-if="operation?.command"
          :command="operation.command"
        />

        <ClientRequestMetadata
          v-if="state.requestMetadata"
          :request-metadata="state.requestMetadata"
        />

        <div v-if="operation?.response?.result">
          <SectionHeading>Execution Stats</SectionHeading>
          <ExecutionStats
            v-if="auxiliaryMetadataList.length > 0"
            :auxiliary-metadata-list="auxiliaryMetadataList"
          />
          <p
            v-else
            class="muted"
          >
            Worker didn't report any execution statistics.
          </p>
        </div>
      </div>

      <div class="column-narrow sticky">
        <div v-if="operation.response?.result">
          <OperationTimeline :operation="operation" />
        </div>
      </div>
    </MultiColumn>
  </LayoutWide>
</template>

<style scoped lang="scss">
:deep(header) {
  padding: 0 4rem;
  margin: 1rem 0 0 0;
}

:deep(.multicolumn) {
  padding: 1rem 4rem;
  background-color: var(--color-bg-bottom);
  box-shadow: 0 0.125rem .5rem rgba(0, 0, 0, .05) inset;
}
</style>
