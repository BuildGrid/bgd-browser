<!--
  Copyright 2024 Bloomberg LP

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<script setup lang="ts">
import { DocumentDuplicateIcon } from "@heroicons/vue/solid";
import { computed, onMounted, reactive, ref, watch } from 'vue';
import { useRoute } from 'vue-router';

import { DecodedAction, DecodedActionResult, getResult } from '@/apis/actions';
import bytestream from '@/apis/bytestream';
import operations, { DecodedOperation } from '@/apis/operations';
import ActionTreeView from "@/components/ActionTreeView.vue";
import Breadcrumbs from '@/components/Breadcrumbs.vue';
import ClientIdentityPills from '@/components/ClientIdentityPills.vue';
import ClientRequestMetadata from '@/components/ClientRequestMetadata.vue';
import CommandMetadata from '@/components/CommandMetadata.vue';
import EmptyView from '@/components/EmptyView.vue';
import ExecutionMetadata from '@/components/ExecutionMetadata.vue';
import ExecutionStats from '@/components/ExecutionStats.vue';
import LayoutWide from '@/components/LayoutWide.vue';
import LogViewer from '@/components/LogViewer.vue';
import MultiColumn from "@/components/MultiColumn.vue";
import OperationTimeline from '@/components/OperationTimeline.vue';
import SectionHeading from '@/components/SectionHeading.vue';
import ViewHeading from '@/components/ViewHeading.vue';
import { Command, Directory, RequestMetadata } from '@/protos/build/bazel/remote/execution/v2/remote_execution';
import { ClientIdentity } from '@/protos/buildgrid/v2/identity';
import { useOperationsCache } from '@/stores/operations';
import axios, { AxiosError } from "axios";

interface State {
  action?: DecodedAction;
  actionResult?: DecodedActionResult;
  breadcrumbs: Array<{ title: string, target: string }>;
  command?: Command;
  error?: Error;
  inputRoot?: Directory;
  loading: boolean;
  notFound: boolean;
  operations: DecodedOperation[];
  requestError?: AxiosError;
  requestMetadata?: RequestMetadata;
  clientIdentity?: ClientIdentity;
}

const route = useRoute()
const state: State = reactive({
  action: undefined,
  actionResult: undefined,
  breadcrumbs: [{ title: 'Operations', target: '/browse' }],
  error: undefined,
  inputRoot: undefined,
  loading: true,
  notFound: false,
  operations: [],
  requestMetadata: undefined,
  clientIdentity: undefined
})

const namespace = (route.params.namespace || '') as string;
const hash = route.params.hash as string;
const sizeBytes = route.params.sizeBytes as string;

const operationName = ref(route.query.operation as string);
const operationsCache = useOperationsCache();
const operation = computed(() => operationsCache.operations.get(operationName.value));
const result = computed(() => {
  return state.actionResult || operation.value?.response?.result;
});

const auxiliaryMetadataList = computed(() => operation.value?.response?.result?.executionMetadata?.auxiliaryMetadata || []);

const copiedDigest = ref(false);
function copyDigest() {
  navigator.clipboard.writeText(`${hash}/${sizeBytes}`)
  copiedDigest.value = true
  setTimeout(() => copiedDigest.value = false, 1000)
}

function operationCanChangeState(op: DecodedOperation | undefined) {
  return op && !operations.operationIsCompleted(op) && !operations.operationWasCancelled(op)
}

const decodedStatusText = computed(() => state.requestError?.response?.statusText ? decodeURI(state.requestError?.response?.statusText) : '')

// TODO: Once the Action view uses the Composition API this should
// be extracted into a composable for reuse.
let refreshTimer: NodeJS.Timeout | undefined = undefined;

function refreshOperation() {
  operationsCache.fetchOperation(namespace, operationName.value).then(operation => {
    if (!operationCanChangeState(operation) && refreshTimer) {
      clearInterval(refreshTimer)
      console.log(`Auto refresh stopped for ${operationName.value}`)
    }
  }).catch(e => {
    state.error = e
  })
}

watch(operation, (current, old) => {
  // Theoretical case where the operation has disappeared for some
  // reason. This shouldn't happen under normal operation.
  if (!current) {
    console.log(`Operation ${operationName.value} became undefined`)
    return
  }

  // If we don't have notification permission, or there is nothing
  // useful to compare against, there's no need to continue
  if (Notification.permission !== 'granted' || !old) {
    return
  }

  const oldStage = operations.stageName(old)
  const newStage = operations.stageName(current)
  if (oldStage === newStage) {
    return
  }

  const notification = new Notification(`Operation ${newStage}`, {
    body: `${current.name}`,
    icon: `${import.meta.env.BASE_URL}bgd-logo-black.png`
  })
  setTimeout(() => notification.close(), 10000)
})

async function fetchData() {
  try {
    state.action = await bytestream.readAction(namespace, hash, sizeBytes);
  } catch (e) {
    state.notFound = true;
    if (axios.isAxiosError(e)) {
      state.requestError = e as AxiosError;
    }
    state.error = e as Error;
  }

  try {
    const ops = await operations.list(namespace, `action_digest = ${hash}/${sizeBytes}`);
    state.operations = ops.operations;
  } catch (_) { }

  const asyncFetches = [];

  if (state.action?.commandDigest) {
    const commandFetch = bytestream.readCommand(namespace, state.action.commandDigest.hash, state.action.commandDigest.sizeBytes);
    commandFetch.then(command => { state.command = command });
    asyncFetches.push(commandFetch);
  }

  if (state.action?.inputRootDigest) {
    const inputFetch = bytestream.readDirectory(namespace, state.action.inputRootDigest.hash, state.action.inputRootDigest.sizeBytes);
    inputFetch.then(root => { state.inputRoot = root });
    asyncFetches.push(inputFetch);
  }

  const resultFetch = getResult(namespace, hash, sizeBytes)
  resultFetch.then(result => { state.actionResult = result });
  asyncFetches.push(resultFetch)

  if (!operationName.value) {
    if (state.operations.length) {
      operationsCache.storeOperation(namespace, state.operations[0])
      operationName.value = state.operations[0].name

      if (!refreshTimer && operationCanChangeState(state.operations[0])) {
        Notification.requestPermission();
        refreshTimer = setInterval(refreshOperation, 5000);
        console.log(`Auto refresh started for ${operationName.value}`);
      }
    }
  }

  const operationFetch = operationsCache.fetchOperation(namespace, operationName.value);
  operationFetch.then(o => {
    if (operationCanChangeState(o)) {
      Notification.requestPermission();
      refreshTimer = setInterval(refreshOperation, 5000);
      console.log(`Auto refresh started for ${operationName.value}`);
    }
  });
  asyncFetches.push(operationFetch);

  const metadataFetch = operations.getRequestMetadata(namespace, operationName.value);
  metadataFetch.then(result => { state.requestMetadata = result }).catch(() => { });
  asyncFetches.push(metadataFetch);

  const identityFetch = operations.getClientIdentity(namespace, operationName.value);
  identityFetch.then(result => { state.clientIdentity = result }).catch(() => { });
  asyncFetches.push(identityFetch);

  await Promise.allSettled(asyncFetches);
}

onMounted(() => {
  state.loading = true;
  fetchData().then(() => { state.loading = false });
})
</script>

<template>
  <LayoutWide v-if="state.loading">
    <EmptyView>
      <template #heading>
        <Breadcrumbs :crumbs="state.breadcrumbs" />
        Action
      </template>
      Loading...
    </EmptyView>
  </LayoutWide>

  <LayoutWide v-else-if="!state.loading && !state.action">
    <EmptyView>
      <template #breadcrumbs>
        <Breadcrumbs :crumbs="state.breadcrumbs" />
      </template>
      <template #heading>
        Action couldn't be retrieved
      </template>
      <template
        v-if="state.error"
        #default
      >
        {{ state.error.message }}
      </template>
      <template
        v-if="state.requestError?.response"
        #details
      >
        {{ decodedStatusText }}
      </template>
    </EmptyView>
  </LayoutWide>

  <LayoutWide v-else-if="!state.loading && state.action">
    <header>
      <Breadcrumbs :crumbs="state.breadcrumbs" />
      <ViewHeading>
        <div class="flex-between">
          <div>
            Action
            <span class="muted">
              {{ hash.slice(0, 16) }}.../{{ sizeBytes }}
            </span>
            <span
              class="popover"
              @click="copyDigest()"
            >
              <DocumentDuplicateIcon class="icon" />
              <div class="popover-content">
                {{ copiedDigest ? 'Copied digest' : `${hash}/${sizeBytes}` }}
              </div>
            </span>
          </div>
          <ExecutionMetadata
            :action="state.action"
            :operation="operation"
            :response="operation?.response"
            :result="result"
          />
        </div>
      </ViewHeading>

      <ClientIdentityPills
        v-if="state.clientIdentity"
        :identity="state.clientIdentity"
      />
    </header>

    <MultiColumn>
      <div class="column-narrow-static sticky">
        <ActionTreeView
          :action="state.action"
          :result="result"
          :root="state.inputRoot"
          :operations-list="state.operations"
        />
      </div>
      <div class="column-wide">
        <LogViewer
          :command="state.command"
          :result="result"
          :operation="operation"
        />

        <CommandMetadata :command="state.command" />
        <ClientRequestMetadata
          v-if="state.requestMetadata"
          :request-metadata="state.requestMetadata"
        />

        <div v-if="result">
          <SectionHeading>Execution Stats</SectionHeading>
          <ExecutionStats
            v-if="auxiliaryMetadataList.length > 0"
            :auxiliary-metadata-list="auxiliaryMetadataList"
          />
          <p
            v-else
            class="muted"
          >
            Worker didn't report any execution statistics.
          </p>
        </div>
      </div>

      <div class="column-narrow sticky">
        <OperationTimeline :operation="operation" />
      </div>
    </MultiColumn>
  </LayoutWide>
</template>

<style lang="scss" scoped>
:deep(header) {
  padding: 0 4rem;
  margin: 1rem 0 0 0;
}

h1 .popover {
  font-size: 1rem;
  position: relative;
  color: var(--color-text-muted);
  padding: 0.25rem;

  .popover-content {
    display: none;
    background-color: var(--color-neutral-dark);
    color: var(--color-white);
    padding: 0.5rem;
    border-radius: 0.5rem;
  }

  &:hover {
    color: var(--color-neutral-dark);
    cursor: pointer;

    .popover-content {
      display: flex;
      position: absolute;
      bottom: 100%;
      left: 0;
      white-space: nowrap;
      cursor: auto;
    }
  }

  .icon {
    height: 1.25rem;
    margin-left: 0.5rem;
  }
}

:deep(.multicolumn) {
  padding: 1rem 4rem;
  background-color: var(--color-bg-bottom);
  box-shadow: 0 0.125rem .5rem rgba(0, 0, 0, .05) inset;
}
</style>
