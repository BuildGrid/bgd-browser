/* eslint-disable no-useless-call */
import * as logger from 'winston';

export function initLogger() {
  logger.configure({
    level: 'info',
    transports: [new logger.transports.Console()],
    rejectionHandlers: [new logger.transports.Console()],
    exceptionHandlers: [new logger.transports.Console()],
  });

  // Override console functions to point to winston
  console.log = (...args: any) => logger.info.call(logger, args);
  console.info = (...args: any) => logger.info.call(logger, args);
  console.error = (...args: any) => logger.error.call(logger, args);
  console.warn = (...args: any) => logger.warn.call(logger, args);
}
