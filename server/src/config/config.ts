import { readFile } from 'fs/promises';
import path from 'path';
import YAML from 'yaml';

export interface GrpcConnectionConfig {
  url: string;
  instanceName: string;
  authTokenPath?: string;
  sslRootCertPath?: string;
}

export interface NamespaceConfig {
  allowOperationCancelling?: boolean;
  remote: GrpcConnectionConfig;

  // Optional service-specific connection config.
  // These override `remote` for a specific client.
  actioncache?: GrpcConnectionConfig;
  buildEvents?: GrpcConnectionConfig;
  cas?: GrpcConnectionConfig;
  introspection?: GrpcConnectionConfig;
  logstream?: GrpcConnectionConfig;
  operations?: GrpcConnectionConfig;
}

export interface Configuration {
  port: number;
  build: boolean;
  env: 'local' | 'cloud';
  appType: 'dev' | 'prod';
  namespaces: { [key: string]: NamespaceConfig }
}

let cachedConfig: Configuration | undefined = undefined;

/**
 * Parse out environment variables to sane values.
 */
export async function loadConfiguration(): Promise<Configuration> {
  const configPath = process.env.CONFIG_PATH || path.join(__dirname, '../../public/config.yaml');
  const configData = await readFile(configPath, { encoding: 'utf8' });
  const config: Configuration = YAML.parse(configData);

  cachedConfig = {
    port: Number(process.env.PORT) || config.port || 5001,
    build: !!process.env.BUILD || config.build || true,
    env: (process.env.ENV || config.env || 'local') as 'local' | 'cloud',
    appType: (process.env.APP_TYPE || config.appType || 'dev') as 'dev' | 'prod',
    namespaces: config.namespaces
  };
  return cachedConfig;
}

export async function getNamespaceConfig(namespace?: string): Promise<NamespaceConfig | undefined> {
  // Handle a totally missing namespace by defaulting to the empty string
  // TODO: Does this still make sense?
  namespace = namespace || '';

  if (cachedConfig !== undefined) {
    return cachedConfig.namespaces[namespace];
  }

  const config = await loadConfiguration();
  return config.namespaces[namespace];
}
