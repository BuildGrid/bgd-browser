import { Handler } from 'express';
import * as logger from 'winston';

export const loggingMiddleware: Handler = (req, res, next) => {
  if (!req.url.startsWith('/api') && !(req.url === '/' || req.url === '')) {
    next();
    return;
  }
  const requestStart = new Date();

  res.on('finish', () => {
    const requestEnd = new Date();

    logger.info(
      `Delivered response http_method=${req.method} uri_path=${req.url
      } status=${res.statusCode} bytes_out=${req.socket.bytesWritten
      } runtime_ms=${requestEnd.getTime() - requestStart.getTime()
      } http_remote_address=${req.ip}`
    );
  });

  next();
};
