import { Handler } from "express";
import { NamespaceConfig, getNamespaceConfig } from "../config/config";

declare global {
  namespace Express {
    interface Request {
      namespace: string;
      config: NamespaceConfig
    }
  }
}

export type namespaceParam = { namespace: string };

export const namespaceMiddleware: Handler = async (req, res, next) => {
  const { namespace } = req.params as typeof req.params & namespaceParam;
  const config = await getNamespaceConfig(namespace);
  if (config === undefined) {
    res.status(404);
    res.send(`Namespace not found: ${namespace}`);
    return;
  }
  req.namespace = namespace;
  req.config = config;
  next();
}
