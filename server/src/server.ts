#!/usr/bin/env node

import express from 'express';
import expressWs from 'express-ws';
import { createServer } from 'http';
import path from 'path';
import * as logger from 'winston';

import { initLogger, loadConfiguration } from './config';
import { loggingMiddleware } from './middleware';

// Set up express and express-ws before importing the routers.
// This allows express-ws to modify the router prototype so that
// the `Router.ws` method it adds is correctly available.
const app = express();
const server = createServer(app);
const expressws = expressWs(app, server);

import actionCache from './routes/action_cache';
import buildEvents from './routes/build_events';
import cas from './routes/cas';
import config from './routes/config';
import introspection from './routes/introspection';
import logstream from './routes/logstream';
import operations from './routes/operations';

initLogger();

async function main(): Promise<void> {
  const appConfig = await loadConfiguration();
  app.set('port', appConfig.port);
  app.enable('trust proxy');
  if (appConfig.env !== 'local') {
    app.set('trust proxy', 1);
  }

  app.use(loggingMiddleware);

  app.use(express.static(path.join(__dirname, '../dist'), {
    setHeaders: (res, path) => {
      if (path.endsWith('index.html')) {
        // Stop browsers caching index.html, since it contains paths to
        // JS files whose paths change on a new build, causing page load
        // failures if index.html is still in cache.
        res.set('Cache-Control', 'no-cache, no-store, must-revalidate');
      }
    }
  }));

  // gRPC API implementations
  app.use('/:namespace?/api/v1/action_results', actionCache);
  app.use('/:namespace?/api/v1/build_events', buildEvents);
  app.use('/:namespace?/api/v1/blobs', cas);
  app.use('/api/config', config);
  app.use('/:namespace?/api/v1/operations', operations);
  app.use('/:namespace?/api/v1', introspection);
  app.use('/:namespace?/ws', logstream)

  app.get('/*', (_, res) => res.sendFile(path.join(__dirname, '../dist', 'index.html'), {
    headers: { 'Cache-Control': 'no-cache, no-store, must-revalidate' }
  }));

  server.listen(appConfig.port, '0.0.0.0', () => {
    logger.info(`ENV = ${appConfig.env}`);
    logger.info(`Server listening PORT = ${app.get('port') as string}`);
  });
}

main().catch((message) => logger.error(`Server shut down: ${String(message)}`));
