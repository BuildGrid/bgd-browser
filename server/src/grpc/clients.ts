
import * as fs from 'fs';

import { Metadata, credentials } from '@grpc/grpc-js';
import { CallMetadataGenerator } from '@grpc/grpc-js/build/src/call-credentials';
import { GrpcTransport } from "@protobuf-ts/grpc-transport";
import { RpcMetadata, RpcOptions } from "@protobuf-ts/runtime-rpc";
import { GrpcConnectionConfig } from "../config/config";
import { RequestMetadata, ToolDetails } from '../protos/build/bazel/remote/execution/v2/remote_execution';
import { ActionCacheClient } from '../protos/build/bazel/remote/execution/v2/remote_execution.client';
import { IntrospectionClient } from '../protos/buildgrid/v2/introspection.client';
import { QueryBuildEventsClient } from '../protos/buildgrid/v2/query_build_events.client';
import { ByteStreamClient } from '../protos/google/bytestream/bytestream.client';
import { OperationsClient } from '../protos/google/longrunning/operations.client';
import { VERSION } from '../version';

const actionCacheClients: { [namespace: string]: { client: ActionCacheClient, expiry: number } } = {};
const byteStreamClients: { [namespace: string]: { client: ByteStreamClient, expiry: number } } = {};
const introspectionClients: { [namespace: string]: { client: IntrospectionClient, expiry: number } } = {};
const queryBuildEventsClients: { [namespace: string]: { client: QueryBuildEventsClient, expiry: number } } = {};
const operationsClients: { [namespace: string]: { client: OperationsClient, expiry: number } } = {};

const CLIENT_TTL = 1000 * 60 * 60;
export const REQUEST_METADATA_KEY = 'build.bazel.remote.execution.v2.requestmetadata-bin';

function generateRequestMetadata() {
  const metadata = RequestMetadata.create({
    toolDetails: ToolDetails.create({
      toolName: "bgd-browser",
      toolVersion: VERSION,
    }),
  })
  return Buffer.from(RequestMetadata.toBinary(metadata)).toString('base64');
}

export function getRpcOptions(): RpcOptions {
  const meta: RpcMetadata = {};
  meta[REQUEST_METADATA_KEY] = generateRequestMetadata();
  return { meta };
}

function transportForUrl(config: GrpcConnectionConfig) {
  let creds = credentials.createInsecure();
  if (config.sslRootCertPath) {
    // TODO: Client certificate support?
    const rootCert = fs.readFileSync(config.sslRootCertPath);
    creds = credentials.createSsl(rootCert);
    if (config.authTokenPath) {
      const token = fs.readFileSync(config.authTokenPath).toString();
      const authMetadataCallback: CallMetadataGenerator = (_params, callback) => {
        const metadata = new Metadata();
        metadata.add('authorization', `Bearer ${token}`);
        callback(null, metadata);
      }
      const authCreds = credentials.createFromMetadataGenerator(authMetadataCallback);
      creds = credentials.combineChannelCredentials(creds, authCreds);
    }
  }

  return new GrpcTransport({
    host: config.url,
    channelCredentials: creds
  });
}

function cacheKey(namespace: string, config: GrpcConnectionConfig): string {
  return `${namespace}-${config.url}`;
}

export function getActionCacheClient(namespace: string, config: GrpcConnectionConfig) {
  const key = cacheKey(namespace, config);
  const cacheEntry = actionCacheClients[key];
  if (!cacheEntry || cacheEntry.expiry >= Date.now()) {
    actionCacheClients[key] = {
      client: new ActionCacheClient(transportForUrl(config)),
      expiry: Date.now() + CLIENT_TTL
    };
  }
  return actionCacheClients[key].client;
}

export function getByteStreamClient(namespace: string, config: GrpcConnectionConfig) {
  const key = cacheKey(namespace, config);
  const cacheEntry = byteStreamClients[key];
  if (!cacheEntry || cacheEntry.expiry >= Date.now()) {
    byteStreamClients[key] = {
      client: new ByteStreamClient(transportForUrl(config)),
      expiry: Date.now() + CLIENT_TTL
    };
  }
  return byteStreamClients[key].client;
}

export function getIntrospectionClient(namespace: string, config: GrpcConnectionConfig) {
  const key = cacheKey(namespace, config);
  const cacheEntry = introspectionClients[key];
  if (!cacheEntry || cacheEntry.expiry >= Date.now()) {
    introspectionClients[key] = {
      client: new IntrospectionClient(transportForUrl(config)),
      expiry: Date.now() + CLIENT_TTL
    };
  }
  return introspectionClients[key].client;
}

export function getQueryBuildEventsClient(namespace: string, config: GrpcConnectionConfig) {
  const key = cacheKey(namespace, config);
  const cacheEntry = queryBuildEventsClients[key];
  if (!cacheEntry || cacheEntry.expiry >= Date.now()) {
    queryBuildEventsClients[key] = {
      client: new QueryBuildEventsClient(transportForUrl(config)),
      expiry: Date.now() + CLIENT_TTL
    };
  }
  return queryBuildEventsClients[key].client;
}

export function getOperationsClient(namespace: string, config: GrpcConnectionConfig) {
  const key = cacheKey(namespace, config);
  const cacheEntry = operationsClients[key];
  if (!cacheEntry || cacheEntry.expiry >= Date.now()) {
    operationsClients[key] = {
      client: new OperationsClient(transportForUrl(config)),
      expiry: Date.now() + CLIENT_TTL
    };
  }
  return operationsClients[key].client;
}
