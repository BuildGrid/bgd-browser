import { ServiceError, status } from '@grpc/grpc-js';
import { MessageType } from "@protobuf-ts/runtime";
import { Response } from "express";
import * as logger from 'winston';

export function messageToBuffer<T extends object>(messageType: MessageType<T>, message: T) {
  return Buffer.from(messageType.toBinary(message));
}

export async function wrappedCall<T>(cb: () => Promise<T>, res: Response): Promise<T | null | undefined> {
  try {
    return await cb();
  } catch (err) {
    logger.error(err);
    if (err instanceof Error) {
      const grpcError = err as ServiceError;

      // This ugly typing is because `grpcError.code` is a String here at
      // runtime, not a status as annotated.
      const statusCode = status[grpcError.code] as unknown as Number;
      switch (statusCode) {
        case status.NOT_FOUND:
          res.status(404);
          break;
        case status.PERMISSION_DENIED:
          res.status(401);
          break;
        case status.FAILED_PRECONDITION:
          res.status(412);
          break;
        case status.OUT_OF_RANGE:
          res.status(400);
          break;
        case status.INVALID_ARGUMENT:
          res.status(400);
          break;
        default:
          res.status(500);
          break;
      }
      res.statusMessage = encodeURI(grpcError.message);
      res.send(grpcError.message)
    } else {
      res.status(500);
      res.send(`Unexpected error in the bgd-browser backend: ${err}`)
    }
    return null;
  }
}
