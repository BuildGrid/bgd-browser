import { Router } from 'express';
import { loadConfiguration } from '../config/config';

const router = Router();

router.get("/", async (req, res) => {
    const config = await loadConfiguration();
    if (config !== undefined) {
        res.send(JSON.stringify(config));
    } else {
        res.send("Failed to load configuration");
        res.status(404);
    }
})

export default router;
