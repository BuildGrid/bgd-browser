import { Router } from 'express';
import { getQueryBuildEventsClient, getRpcOptions } from '../grpc/clients';
import { messageToBuffer, wrappedCall } from '../grpc/utils';
import { namespaceMiddleware } from '../middleware';
import { QueryEventStreamsRequest, QueryEventStreamsResponse } from '../protos/buildgrid/v2/query_build_events';

const router = Router({ mergeParams: true });
router.use(namespaceMiddleware);

router.get('/', async (req, res) => {
  const config = req.config.buildEvents || req.config.remote;
  const grpcRequest = QueryEventStreamsRequest.create();
  if (req.query.build_id) {
    grpcRequest.buildIdPattern = req.query.build_id as string;
  }

  const client = getQueryBuildEventsClient(req.namespace, config);
  const grpcResponse = await wrappedCall(async () => await client.queryEventStreams(grpcRequest, getRpcOptions()).response, res);
  if (grpcResponse !== undefined && grpcResponse !== null) {
    res.send(messageToBuffer(QueryEventStreamsResponse, grpcResponse));
  }
});

export default router;
