import { Router } from 'express';
import { getOperationsClient, getRpcOptions, REQUEST_METADATA_KEY } from '../grpc/clients';
import { messageToBuffer, wrappedCall } from '../grpc/utils';
import { namespaceMiddleware } from '../middleware';
import {
  CancelOperationRequest,
  GetOperationRequest,
  ListOperationsRequest,
  ListOperationsResponse,
  Operation
} from '../protos/google/longrunning/operations';

const CLIENT_IDENTITY_KEY = 'buildgrid.v2.clientidentity-bin';

const router = Router({ mergeParams: true });
router.use(namespaceMiddleware)

// List Operations
router.get('/', async (req, res) => {
  const config = req.config.operations || req.config.remote;
  const grpcRequest = ListOperationsRequest.create({ name: config.instanceName });
  if (req.query.q) {
    grpcRequest.filter = req.query.q as string;
  }
  if (req.query.page_token) {
    grpcRequest.pageToken = req.query.page_token as string;
  }
  if (req.query.page_size) {
    const pageSize = parseInt(req.query.page_size as string);
    if (!isNaN(pageSize)) {
      grpcRequest.pageSize = pageSize;
    }
  }

  const client = getOperationsClient(req.namespace, config);
  const grpcResponse = await wrappedCall(async () => await client.listOperations(grpcRequest, getRpcOptions()).response, res);
  if (grpcResponse) {
    res.send(messageToBuffer(ListOperationsResponse, grpcResponse));
  }
});

// Get Operation
router.get('/:name', async (req, res) => {
  const config = req.config.operations || req.config.remote;
  const grpcRequest = GetOperationRequest.create({ name: `${config.instanceName}/${req.params.name}` });
  const client = getOperationsClient(req.namespace, config);
  const grpcResponse = await wrappedCall(async () => await client.getOperation(grpcRequest, getRpcOptions()).response, res);
  if (grpcResponse) {
    res.send(messageToBuffer(Operation, grpcResponse));
  }
});

// Get Operation Request Metadata
router.get('/:name/request_metadata', async (req, res) => {
  const config = req.config.operations || req.config.remote;
  const grpcRequest = GetOperationRequest.create({ name: `${config.instanceName}/${req.params.name}` });
  const client = getOperationsClient(req.namespace, config);
  const trailingMetadata = await wrappedCall(async () => await client.getOperation(grpcRequest, getRpcOptions()).trailers, res);
  if (trailingMetadata) {
    const requestMetadata = trailingMetadata[REQUEST_METADATA_KEY] as string || ''
    const metadataBuffer = Buffer.from(requestMetadata, 'base64');
    res.send(metadataBuffer);
  }
});

// Get Operation client identity metadata
router.get('/:name/client_identity', async (req, res) => {
  const config = req.config.operations || req.config.remote;
  const grpcRequest = GetOperationRequest.create({ name: `${config.instanceName}/${req.params.name}` });
  const client = getOperationsClient(req.namespace, config);
  const trailingMetadata = await wrappedCall(async () => await client.getOperation(grpcRequest, getRpcOptions()).trailers, res);
  if (trailingMetadata) {
    const clientIdentity = trailingMetadata[CLIENT_IDENTITY_KEY] as string || '';
    const metadataBuffer = Buffer.from(clientIdentity, 'base64');
    res.send(metadataBuffer);
  }
})

// Cancel Operation
router.delete('/:name', async (req, res) => {
  const config = req.config.operations || req.config.remote;
  if (req.config.allowOperationCancelling) {
    const grpcRequest = CancelOperationRequest.create({ name: `${config.instanceName}/${req.params.name}` });
    const client = getOperationsClient(req.namespace, config);
    await wrappedCall(async () => await client.cancelOperation(grpcRequest, getRpcOptions()).response, res);
  } else {
    res.status(405);
  }
  res.send();
});

export default router;
