import { Router } from 'express';
import { getActionCacheClient, getRpcOptions } from '../grpc/clients';
import { messageToBuffer, wrappedCall } from '../grpc/utils';
import { namespaceMiddleware } from '../middleware';
import { ActionResult, Digest, GetActionResultRequest } from '../protos/build/bazel/remote/execution/v2/remote_execution';

const router = Router({ mergeParams: true });
router.use(namespaceMiddleware)

router.get('/:hash/:sizeBytes', async (req, res) => {
  const config = req.config.actioncache || req.config.remote;
  const grpcRequest = GetActionResultRequest.create({
    instanceName: config.instanceName
  });
  try {
    grpcRequest.actionDigest = Digest.create({
      hash: req.params.hash,
      sizeBytes: BigInt(req.params.sizeBytes)
    });
  } catch (e) {
    if (e instanceof SyntaxError) {
      res.status(400);
      res.send(e.message);
      return;
    }
  }

  const client = getActionCacheClient(req.namespace, config);
  const grpcResponse = await wrappedCall(async () => await client.getActionResult(grpcRequest, getRpcOptions()).response, res);
  if (grpcResponse !== undefined && grpcResponse !== null) {
    res.send(messageToBuffer(ActionResult, grpcResponse));
  }
});

export default router;
