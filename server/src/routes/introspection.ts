import { Router } from 'express';
import { getIntrospectionClient, getRpcOptions } from '../grpc/clients';
import { messageToBuffer, wrappedCall } from '../grpc/utils';
import { namespaceMiddleware } from '../middleware';
import { GetOperationFiltersRequest, ListWorkersRequest, ListWorkersResponse, OperationFilter } from '../protos/buildgrid/v2/introspection';

const router = Router({ mergeParams: true });
router.use(namespaceMiddleware)

router.get('/operation_filters', async (req, res) => {
  const config = req.config.introspection || req.config.remote;
  const grpcRequest = GetOperationFiltersRequest.create({
    instanceName: config.instanceName
  });

  const grpcResponse = await wrappedCall(async () => {
    const client = getIntrospectionClient(req.namespace, config);
    const call = client.getOperationFilters(grpcRequest, getRpcOptions());
    return await call.response;
  }, res);
  if (grpcResponse) {
    // The frontend expects a JSON array here
    // TODO: update the frontend and this to serialize a proto like the rest of the API,
    // or update the rest of the API to use JSON and TS interfaces
    const filters = grpcResponse.filters.map(filter => OperationFilter.toJson(filter));
    res.send(JSON.stringify(filters));
  }
});

router.get('/workers', async (req, res) => {
  const config = req.config.introspection || req.config.remote;
  const grpcRequest = ListWorkersRequest.create({
    instanceName: config.instanceName
  })
  if (req.query.worker_name) {
    grpcRequest.workerName = req.query.worker_name as string;
  }
  if (req.query.page_size) {
    const pageSize = parseInt(req.query.page_size as string);
    if (!isNaN(pageSize)) {
      grpcRequest.pageSize = pageSize;
    }
  }
  if (req.query.page) {
    const page = parseInt(req.query.page as string);
    if (!isNaN(page)) {
      grpcRequest.page = page;
    }
  }

  const client = getIntrospectionClient(req.namespace, config);
  const grpcResponse = await wrappedCall(async () => await client.listWorkers(grpcRequest, getRpcOptions()).response, res);
  if (grpcResponse) {
    res.send(messageToBuffer(ListWorkersResponse, grpcResponse));
  }
});

export default router;
