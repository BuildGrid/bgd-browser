import { Router } from 'express';
import { getByteStreamClient, getRpcOptions } from '../grpc/clients';
import { wrappedCall } from '../grpc/utils';
import { namespaceMiddleware } from '../middleware';
import { ReadRequest } from '../protos/google/bytestream/bytestream';

const router = Router({ mergeParams: true });
router.use(namespaceMiddleware)

router.get('/:hash/:sizeBytes', async (req, res) => {
  const config = req.config.cas || req.config.remote;
  const { hash, sizeBytes } = req.params;
  const resourceName = `${config.instanceName}/blobs/${hash}/${sizeBytes}`
  const grpcRequest = ReadRequest.create({ resourceName });

  const client = getByteStreamClient(req.namespace, config);
  await wrappedCall(async () => {
    for await (const response of client.read(grpcRequest, getRpcOptions()).responses) {
      res.write(Buffer.from(response.data));
    }
    res.end()
  }, res);
});

export default router;
