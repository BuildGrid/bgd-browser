import { Router } from 'express';
import { getByteStreamClient, getRpcOptions } from '../grpc/clients';
import { namespaceMiddleware } from '../middleware';
import { ReadRequest } from '../protos/google/bytestream/bytestream';

const router = Router({ mergeParams: true });
router.use(namespaceMiddleware)

router.ws('/logstream', (ws, req) => {
  const config = req.config.logstream || req.config.remote;
  ws.on('message', msg => {
    const client = getByteStreamClient(req.namespace, config);
    const grpcRequest = ReadRequest.fromBinary(msg as Buffer);
    const streamLogs = async () => {
      const decoder = new TextDecoder('utf-8');
      for await (const response of client.read(grpcRequest, getRpcOptions()).responses) {
        const wsResponse = {
          complete: false,
          data: decoder.decode(response.data),
          resource_name: grpcRequest.resourceName,
        };
        ws.send(JSON.stringify(wsResponse));
      }
      const finalResponse = {
        complete: true,
        data: new Uint8Array(),
        resource_name: grpcRequest.resourceName,
      };
      ws.send(JSON.stringify(finalResponse));
    }
    streamLogs();
  });
})

export default router;
