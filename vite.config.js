import vue from '@vitejs/plugin-vue'
import { defineConfig } from 'vite'

// Read Git SHA and tag to display in footer:
const getRepoInfo = require('git-repo-info')
const repoInfo = getRepoInfo()
process.env.VITE_GIT_ABBREVIATED_HASH = repoInfo.abbreviatedSha ? repoInfo.abbreviatedSha : ''
process.env.VITE_GIT_TAG = repoInfo.tag ? repoInfo.tag : ''
process.env.VITE_APP_VERSION = process.env.npm_package_version

const path = require('path')
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src')
    }
  },
  server: {
    proxy: {
      '^.*/api/.*': {
        target: 'http://localhost:5001',
        secure: false
      },
      '^.*/ws': {
        target: 'ws://localhost:5001',
        ws: true
      }
    }
  },
  base: process.env.BROWSER_BASE_PATH
    ? process.env.BROWSER_BASE_PATH
    : '/'
})
